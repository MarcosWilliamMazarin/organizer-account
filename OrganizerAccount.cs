﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

namespace Organizer_Account
{
    public partial class OrganizerAccount : Form
    {
        private string DiretorioXMLAtual, diretorioPadrao, usuarioPadrao = "";
        private Lancamentos LancamentosAtual = null;
        private string diretorioConfiguracoes = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\Organizer Account";
        private Lancamento glancamentoSelecionadoAlterar = null;

        public OrganizerAccount()
        {
            InitializeComponent();
            this.diretorioPadrao = Geral.RetornarDiretorioPadrao(diretorioConfiguracoes);
            this.usuarioPadrao = Geral.RetornarUsuarioPadrao(diretorioConfiguracoes);
        }

        #region Acoes Componentes

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            this.AdicionarLancamento();
        }

        private void edtValorLancamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void AssociarDataSourceGrid()
        {
            if (DiretorioXMLAtual != "")
            {
                gridLancamentos.DataSource = Geral.LerXML(DiretorioXMLAtual).Lancamento;
                this.SetarConiguracaoVisualGridLancamentos();
                this.PintarGrid();
            }
            else
            {
                gridLancamentos.DataSource = null;
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.AlterarLancamento();
        }

        private void btnDeletar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Você deseja realmente excluir o lançamento?", "Organizer Account", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) == DialogResult.Yes)
            {
                this.DeletarLancamento();
            }
        }

        private void btnTotalizar_Click(object sender, EventArgs e)
        {
            this.TotalizarValores();
        }

        private void btnNovaAgenda_Click(object sender, EventArgs e)
        {
            this.NovoArquivo();
        }

        private void btnAbrirAgenda_Click(object sender, EventArgs e)
        {
            this.AbrirArquivo();
            if (this.DiretorioXMLAtual != null)
            {
                this.TotalizarValores();
                this.MarcarDatasVencimentos();
                this.VerificarDataVencimento();
            }
        }

        private void btnSalvarAgenda_Click(object sender, EventArgs e)
        {
            Geral.SalvarXML(this.DiretorioXMLAtual, LancamentosAtual);
        }

        private void btnFecharAgendar_Click(object sender, EventArgs e)
        {
            this.FecharArquivo();
        }

        private void btnConfiguracoes_Click(object sender, EventArgs e)
        {
            FormConfiguracoes formConfiguracoes = new FormConfiguracoes();
            formConfiguracoes.StartPosition = FormStartPosition.CenterParent;
            formConfiguracoes.ShowDialog(this);
        }

        #endregion Acoes Componentes

        #region Funcoes com o arquivo

        private void NovoArquivo()
        {
            this.LimparLayout();
            this.LancamentosAtual = Geral.CriarXML(this.RetornarDiretorioSalvar());

            this.AssociarDataSourceGrid();
        }

        private void FecharArquivo()
        {
            this.LancamentosAtual = null;
            this.DiretorioXMLAtual = "";
            this.AssociarDataSourceGrid();
            this.LimparLayout();
        }

        private void AbrirArquivo()
        {
            openFileOrganizerAccount.InitialDirectory = this.diretorioPadrao;
            openFileOrganizerAccount.AddExtension = true;
            openFileOrganizerAccount.DefaultExt = ".xml";
            openFileOrganizerAccount.ShowDialog(this);

            if (openFileOrganizerAccount.FileName != "")
            {
                this.LancamentosAtual = Geral.LerXML(openFileOrganizerAccount.FileName);
                this.DiretorioXMLAtual = openFileOrganizerAccount.FileName;
                this.AssociarDataSourceGrid();
            }
        }

        #endregion Funcoes com o arquivo

        #region Funcoes do layout

        private void MarcarDatasVencimentos()
        {
            try
            {
                this.LimparCalendario();
                if (this.LancamentosAtual != null)
                {
                    foreach (Lancamento l in LancamentosAtual)
                    {
                        calendarioPrincipal.AddBoldedDate(l.dataVencimento);
                    }
                    calendarioPrincipal.UpdateBoldedDates();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Ocorreu um erro ao marcar os dias de lançamentos, reinicie o Organizer Account", "Organizer Account", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void LimparLayout()
        {
            gridLancamentos.DataSource = null;
            this.LimparCampos();
            this.LimparTotalizadores();
            this.LimparCalendario();
            this.LimparLembretes();
        }

        private void LimparTotalizadores()
        {
            lblContadorDebito.Text = lblContadorCredito.Text = lblContadorTotalGeral.Text = "R$";
        }

        private void LimparCampos()
        {
            edtData.Text = edtLancamento.Text = edtValorLancamento.Text = string.Empty;
            cbTipoCredito.ResetText();
        }

        private void SetarConiguracaoVisualGridLancamentos()
        {
            gridLancamentos.Columns["usuario"].HeaderText = "Usuário";
            gridLancamentos.Columns["descricao"].HeaderText = "Descrição do Lançamento";
            gridLancamentos.Columns["valor"].HeaderText = "Valor(R$)";
            gridLancamentos.Columns["dataVencimento"].HeaderText = "Data de Vencimento";
            gridLancamentos.Columns["tipoLancamento"].HeaderText = "Tipo do Lançamento";

            gridLancamentos.Columns["usuario"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            gridLancamentos.Columns["descricao"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            gridLancamentos.Columns["valor"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            gridLancamentos.Columns["dataVencimento"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            gridLancamentos.Columns["tipoLancamento"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            gridLancamentos.Columns["descricao"].DisplayIndex = 0;
            gridLancamentos.Columns["valor"].DisplayIndex = 1;
            gridLancamentos.Columns["tipoLancamento"].DisplayIndex = 2;
            gridLancamentos.Columns["dataVencimento"].DisplayIndex = 3;
            gridLancamentos.Columns["usuario"].DisplayIndex = 4;

            gridLancamentos.Columns["ID"].Visible = false;
        }

        private bool ValidacoesAdicionar()
        {
            return ((edtData.Text != "") && (edtLancamento.Text != "") && (edtValorLancamento.Text != "") && (cbTipoCredito.Text != "") && (this.DiretorioXMLAtual != "") && (this.LancamentosAtual != null));
        }

        private void VincularLancamentoNoLayout(Lancamento lancamentoVincular)
        {
            if (lancamentoVincular != null)
            {
                edtLancamento.Text = lancamentoVincular.descricao.ToString();
                edtValorLancamento.Text = lancamentoVincular.valor.ToString();
                edtData.Text = lancamentoVincular.dataVencimento.ToShortDateString();
                cbTipoCredito.Text = lancamentoVincular.tipoLancamento.ToString();
            }
        }

        private void DefineLayout(string operacao)
        {
            if (operacao == "alterar")
            {
                btnAdicionar.Visible = false;
                btnDeletar.Visible = false;
                btnAlterar.Text = "Salvar";
            }
            else if (operacao == "default")
            {
                btnAdicionar.Visible = true;
                btnDeletar.Visible = true;
                btnAlterar.Text = "Alterar";
            }
        }

        private void LimparCalendario()
        {
            calendarioPrincipal.RemoveAllBoldedDates();
            calendarioPrincipal.UpdateBoldedDates();
        }

        private void PintarGrid()
        {
            int contador = 0;
            while (contador < gridLancamentos.RowCount)
            {
                if (gridLancamentos.Rows[contador].Cells["tipoLancamento"].Value.ToString() == "Crédito")
                {
                    gridLancamentos.Rows[contador].Cells["tipoLancamento"].Style.BackColor = Color.PowderBlue;
                    gridLancamentos.Rows[contador].Cells["valor"].Style.BackColor = Color.PowderBlue;
                    gridLancamentos.Rows[contador].Cells["dataVencimento"].Style.BackColor = Color.PowderBlue;
                    gridLancamentos.Rows[contador].Cells["usuario"].Style.BackColor = Color.PowderBlue;
                    gridLancamentos.Rows[contador].Cells["descricao"].Style.BackColor = Color.PowderBlue;
                }
                else if (gridLancamentos.Rows[contador].Cells["tipoLancamento"].Value.ToString() == "Débito")
                {
                    gridLancamentos.Rows[contador].Cells["tipoLancamento"].Style.BackColor = Color.MistyRose;
                    gridLancamentos.Rows[contador].Cells["valor"].Style.BackColor = Color.MistyRose;
                    gridLancamentos.Rows[contador].Cells["dataVencimento"].Style.BackColor = Color.MistyRose;
                    gridLancamentos.Rows[contador].Cells["usuario"].Style.BackColor = Color.MistyRose;
                    gridLancamentos.Rows[contador].Cells["descricao"].Style.BackColor = Color.MistyRose;
                }
                contador++;
            }
        }

        private void LimparLembretes()
        {
            gridLembretes.DataSource = null;
        }

        private void SetarConiguracaoVisualGridLembretes()
        {
            gridLembretes.Columns["descricao"].HeaderText = "Descrição do Lançamento";
            gridLembretes.Columns["dataVencimento"].HeaderText = "Data de Vencimento";

            gridLembretes.Columns["descricao"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;
            gridLembretes.Columns["dataVencimento"].AutoSizeMode = DataGridViewAutoSizeColumnMode.Fill;

            gridLembretes.Columns["descricao"].DisplayIndex = 0;
            gridLembretes.Columns["dataVencimento"].DisplayIndex = 1;

            gridLembretes.Columns["ID"].Visible = false;
            gridLembretes.Columns["usuario"].Visible = false;
            gridLembretes.Columns["valor"].Visible = false;
            gridLembretes.Columns["tipoLancamento"].Visible = false;
        }

        #endregion Funcoes do layout

        #region Funcoes Gerais

        private void AdicionarLancamento()
        {
            if (this.ValidacoesAdicionar())
            {
                Lancamento lancamentoAdicionar = new Lancamento();

                if (this.LancamentosAtual.Lancamento.Count > 0)
                {
                    lancamentoAdicionar.ID = this.LancamentosAtual.Lancamento[this.LancamentosAtual.Lancamento.Count - 1].ID + 1;
                }
                else
                {
                    lancamentoAdicionar.ID = 1;
                }
                lancamentoAdicionar.descricao = edtLancamento.Text.ToString();
                lancamentoAdicionar.valor = double.Parse(edtValorLancamento.Text);
                lancamentoAdicionar.dataVencimento = DateTime.Parse(edtData.Text);
                lancamentoAdicionar.tipoLancamento = cbTipoCredito.Text.ToString();
                lancamentoAdicionar.usuario = this.usuarioPadrao;

                this.LancamentosAtual.Lancamento.Add(lancamentoAdicionar);
                Geral.SalvarXML(DiretorioXMLAtual, LancamentosAtual);

                this.AssociarDataSourceGrid();
                this.LimparCampos();
                this.TotalizarValores();
                this.MarcarDatasVencimentos();
            }
        }

        private string RetornarDiretorioSalvar()
        {
            saveFileOrganizerAccount.InitialDirectory = diretorioPadrao;
            saveFileOrganizerAccount.AddExtension = true;
            saveFileOrganizerAccount.DefaultExt = ".xml";
            saveFileOrganizerAccount.ShowDialog(this);
            this.DiretorioXMLAtual = saveFileOrganizerAccount.FileName;

            return DiretorioXMLAtual;
        }

        private void DeletarLancamento()
        {
            try
            {
                Lancamento lancamentoDeletar = (Lancamento)gridLancamentos.CurrentRow.DataBoundItem;
                LancamentosAtual.Lancamento.Remove(LancamentosAtual.Lancamento.Find(l => l.ID == lancamentoDeletar.ID));
                Geral.SalvarXML(this.DiretorioXMLAtual, this.LancamentosAtual);
                this.AssociarDataSourceGrid();
                this.TotalizarValores();
                this.MarcarDatasVencimentos();
            }
            catch (Exception)
            {
                MessageBox.Show("É necessário ter um lançamento selecionado para deletar", "Organizer Account", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void AlterarLancamento()
        {
            try
            {
                if (btnAlterar.Text == "Alterar")
                {
                    glancamentoSelecionadoAlterar = (Lancamento)gridLancamentos.CurrentRow.DataBoundItem;
                    this.VincularLancamentoNoLayout(glancamentoSelecionadoAlterar);
                    this.DefineLayout("alterar");
                }
                else if (btnAlterar.Text == "Salvar")
                {
                    Lancamento lancamentoAlterar = this.LancamentosAtual.Lancamento.Find(l => l.ID == glancamentoSelecionadoAlterar.ID);

                    lancamentoAlterar.descricao = edtLancamento.Text.ToString();
                    lancamentoAlterar.valor = double.Parse(edtValorLancamento.Text);
                    lancamentoAlterar.dataVencimento = DateTime.Parse(edtData.Text);
                    lancamentoAlterar.tipoLancamento = cbTipoCredito.Text.ToString();

                    Geral.SalvarXML(this.DiretorioXMLAtual, this.LancamentosAtual);
                    this.DefineLayout("default");
                    this.AssociarDataSourceGrid();
                    this.TotalizarValores();
                    this.MarcarDatasVencimentos();
                }
            }
            catch (Exception)
            {
                MessageBox.Show("É necessário ter um lançamento selecionado para alterar", "Organizer Account", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void TotalizarValores()
        {
            try
            {
                double totalCredito, totalDebito, totalGeral;
                totalCredito = totalDebito = totalGeral = 0;
                foreach (Lancamento l in this.LancamentosAtual)
                {
                    if (l.tipoLancamento == "Crédito")
                    {
                        totalCredito += l.valor;
                    }
                    else if (l.tipoLancamento == "Débito")
                    {
                        totalDebito += l.valor;
                    }
                }
                totalGeral = totalCredito - totalDebito;

                if (totalGeral > 0)
                {
                    lblContadorTotalGeral.ForeColor = Color.DarkGreen;
                }
                else if (totalGeral < 0)
                {
                    lblContadorTotalGeral.ForeColor = Color.Crimson;
                }

                lblContadorCredito.Text = "R$" + totalCredito.ToString();
                lblContadorDebito.Text = "R$" + totalDebito.ToString();
                lblContadorTotalGeral.Text = "R$" + totalGeral.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("É necessário ter um arquivo carregado para totalizar valores", "Organizer Account", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
        }

        private void btnPesquisarLancamento_Click(object sender, EventArgs e)
        {
            Pesquisa formPesquisa = new Pesquisa(this.LancamentosAtual);
            formPesquisa.ShowDialog(this);
        }

        

        private void VerificarDataVencimento()
        {
            double diasAviso = Geral.RetornarDiasAviso(diretorioConfiguracoes);
            List<Lancamento> lancamentosAVencer = new List<Lancamento>();

            foreach (Lancamento l in this.LancamentosAtual)
            {
                if ((DateTime.Compare(l.dataVencimento.Date.ToLocalTime(), DateTime.Today.AddDays(diasAviso).ToLocalTime()) < 0)
                    || (DateTime.Compare(l.dataVencimento.Date.ToLocalTime(), DateTime.Today.AddDays(diasAviso).ToLocalTime()) == 0) &&
                    (l.tipoLancamento != "Crédito"))
                {
                    lancamentosAVencer.Add(l);
                }
            }

            if (lancamentosAVencer != null)
            {
                gridLembretes.DataSource = lancamentosAVencer;
                this.SetarConiguracaoVisualGridLembretes();
                MessageBox.Show("Foram encontrados lançamentos com a data de vencimento se aproximando, verifique a aba 'Lembretes'", "Organizer Account", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        #endregion Funcoes Gerais
    }
}