﻿namespace Organizer_Account
{
    partial class OrganizerAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrganizerAccount));
            this.pnlGridLancamentos = new System.Windows.Forms.Panel();
            this.tsFuncionalidades = new System.Windows.Forms.ToolStrip();
            this.btnPesquisarLancamento = new System.Windows.Forms.ToolStripButton();
            this.gridLancamentos = new System.Windows.Forms.DataGridView();
            this.lblTotalizadores = new System.Windows.Forms.Label();
            this.lblTotalCredito = new System.Windows.Forms.Label();
            this.lblTotalDebito = new System.Windows.Forms.Label();
            this.lblTotalGeral = new System.Windows.Forms.Label();
            this.lblContadorCredito = new System.Windows.Forms.Label();
            this.lblContadorDebito = new System.Windows.Forms.Label();
            this.lblContadorTotalGeral = new System.Windows.Forms.Label();
            this.pnlTotalizadores = new System.Windows.Forms.Panel();
            this.pgcTotalizadoresAvisos = new System.Windows.Forms.TabControl();
            this.tabTotalizadores = new System.Windows.Forms.TabPage();
            this.label1 = new System.Windows.Forms.Label();
            this.btnTotalizar = new System.Windows.Forms.Button();
            this.tabLembretes = new System.Windows.Forms.TabPage();
            this.gridLembretes = new System.Windows.Forms.DataGridView();
            this.pnlDescricaoLancamento = new System.Windows.Forms.Panel();
            this.lblLancamento = new System.Windows.Forms.Label();
            this.tsMenuPrincipal = new System.Windows.Forms.ToolStrip();
            this.btnAgenda = new System.Windows.Forms.ToolStripSplitButton();
            this.btnNovaAgenda = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAbrirAgenda = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSalvarAgenda = new System.Windows.Forms.ToolStripMenuItem();
            this.btnFecharAgendar = new System.Windows.Forms.ToolStripMenuItem();
            this.btnConfiguracoes = new System.Windows.Forms.ToolStripButton();
            this.edtData = new System.Windows.Forms.DateTimePicker();
            this.lblDataLancamento = new System.Windows.Forms.Label();
            this.btnDeletar = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnAdicionar = new System.Windows.Forms.Button();
            this.cbTipoCredito = new System.Windows.Forms.ComboBox();
            this.lblTipoLancamento = new System.Windows.Forms.Label();
            this.edtValorLancamento = new System.Windows.Forms.TextBox();
            this.lblValor = new System.Windows.Forms.Label();
            this.edtLancamento = new System.Windows.Forms.TextBox();
            this.saveFileOrganizerAccount = new System.Windows.Forms.SaveFileDialog();
            this.openFileOrganizerAccount = new System.Windows.Forms.OpenFileDialog();
            this.calendarioPrincipal = new System.Windows.Forms.MonthCalendar();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pnlCalendario = new System.Windows.Forms.Panel();
            this.pnlGridLancamentos.SuspendLayout();
            this.tsFuncionalidades.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLancamentos)).BeginInit();
            this.pnlTotalizadores.SuspendLayout();
            this.pgcTotalizadoresAvisos.SuspendLayout();
            this.tabTotalizadores.SuspendLayout();
            this.tabLembretes.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLembretes)).BeginInit();
            this.pnlDescricaoLancamento.SuspendLayout();
            this.tsMenuPrincipal.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.pnlCalendario.SuspendLayout();
            this.SuspendLayout();
            // 
            // pnlGridLancamentos
            // 
            this.pnlGridLancamentos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGridLancamentos.Controls.Add(this.tsFuncionalidades);
            this.pnlGridLancamentos.Controls.Add(this.gridLancamentos);
            this.pnlGridLancamentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlGridLancamentos.Location = new System.Drawing.Point(3, 173);
            this.pnlGridLancamentos.Name = "pnlGridLancamentos";
            this.pnlGridLancamentos.Size = new System.Drawing.Size(707, 368);
            this.pnlGridLancamentos.TabIndex = 2;
            // 
            // tsFuncionalidades
            // 
            this.tsFuncionalidades.BackColor = System.Drawing.Color.GhostWhite;
            this.tsFuncionalidades.Dock = System.Windows.Forms.DockStyle.Left;
            this.tsFuncionalidades.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.tsFuncionalidades.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnPesquisarLancamento});
            this.tsFuncionalidades.Location = new System.Drawing.Point(0, 0);
            this.tsFuncionalidades.Name = "tsFuncionalidades";
            this.tsFuncionalidades.ShowItemToolTips = false;
            this.tsFuncionalidades.Size = new System.Drawing.Size(24, 366);
            this.tsFuncionalidades.TabIndex = 1;
            this.tsFuncionalidades.Text = "tsFuncionalidades";
            // 
            // btnPesquisarLancamento
            // 
            this.btnPesquisarLancamento.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnPesquisarLancamento.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisarLancamento.Image")));
            this.btnPesquisarLancamento.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnPesquisarLancamento.Name = "btnPesquisarLancamento";
            this.btnPesquisarLancamento.Size = new System.Drawing.Size(21, 20);
            this.btnPesquisarLancamento.Text = "toolStripButton1";
            this.btnPesquisarLancamento.ToolTipText = "btnPesquisarLancamento";
            this.btnPesquisarLancamento.Click += new System.EventHandler(this.btnPesquisarLancamento_Click);
            // 
            // gridLancamentos
            // 
            this.gridLancamentos.AllowUserToAddRows = false;
            this.gridLancamentos.AllowUserToDeleteRows = false;
            this.gridLancamentos.AllowUserToOrderColumns = true;
            this.gridLancamentos.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.gridLancamentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridLancamentos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridLancamentos.Location = new System.Drawing.Point(0, 0);
            this.gridLancamentos.Name = "gridLancamentos";
            this.gridLancamentos.ReadOnly = true;
            this.gridLancamentos.Size = new System.Drawing.Size(705, 366);
            this.gridLancamentos.TabIndex = 0;
            // 
            // lblTotalizadores
            // 
            this.lblTotalizadores.AutoSize = true;
            this.lblTotalizadores.Font = new System.Drawing.Font("Hack", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalizadores.Location = new System.Drawing.Point(34, 5);
            this.lblTotalizadores.Name = "lblTotalizadores";
            this.lblTotalizadores.Size = new System.Drawing.Size(139, 19);
            this.lblTotalizadores.TabIndex = 0;
            this.lblTotalizadores.Text = "Totalizadores";
            // 
            // lblTotalCredito
            // 
            this.lblTotalCredito.AutoSize = true;
            this.lblTotalCredito.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCredito.Location = new System.Drawing.Point(3, 48);
            this.lblTotalCredito.Name = "lblTotalCredito";
            this.lblTotalCredito.Size = new System.Drawing.Size(126, 13);
            this.lblTotalCredito.TabIndex = 1;
            this.lblTotalCredito.Text = "Total de Crédito:";
            // 
            // lblTotalDebito
            // 
            this.lblTotalDebito.AutoSize = true;
            this.lblTotalDebito.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDebito.Location = new System.Drawing.Point(10, 66);
            this.lblTotalDebito.Name = "lblTotalDebito";
            this.lblTotalDebito.Size = new System.Drawing.Size(119, 13);
            this.lblTotalDebito.TabIndex = 2;
            this.lblTotalDebito.Text = "Total de Débito:";
            // 
            // lblTotalGeral
            // 
            this.lblTotalGeral.AutoSize = true;
            this.lblTotalGeral.Font = new System.Drawing.Font("Hack", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalGeral.Location = new System.Drawing.Point(26, 95);
            this.lblTotalGeral.Name = "lblTotalGeral";
            this.lblTotalGeral.Size = new System.Drawing.Size(103, 15);
            this.lblTotalGeral.TabIndex = 3;
            this.lblTotalGeral.Text = "Total Geral:";
            // 
            // lblContadorCredito
            // 
            this.lblContadorCredito.AutoSize = true;
            this.lblContadorCredito.BackColor = System.Drawing.Color.Transparent;
            this.lblContadorCredito.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContadorCredito.ForeColor = System.Drawing.Color.Black;
            this.lblContadorCredito.Location = new System.Drawing.Point(126, 48);
            this.lblContadorCredito.Name = "lblContadorCredito";
            this.lblContadorCredito.Size = new System.Drawing.Size(21, 13);
            this.lblContadorCredito.TabIndex = 4;
            this.lblContadorCredito.Text = "R$";
            // 
            // lblContadorDebito
            // 
            this.lblContadorDebito.AutoSize = true;
            this.lblContadorDebito.BackColor = System.Drawing.Color.Transparent;
            this.lblContadorDebito.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContadorDebito.ForeColor = System.Drawing.Color.Black;
            this.lblContadorDebito.Location = new System.Drawing.Point(126, 66);
            this.lblContadorDebito.Name = "lblContadorDebito";
            this.lblContadorDebito.Size = new System.Drawing.Size(21, 13);
            this.lblContadorDebito.TabIndex = 5;
            this.lblContadorDebito.Text = "R$";
            // 
            // lblContadorTotalGeral
            // 
            this.lblContadorTotalGeral.AutoSize = true;
            this.lblContadorTotalGeral.BackColor = System.Drawing.Color.Transparent;
            this.lblContadorTotalGeral.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContadorTotalGeral.ForeColor = System.Drawing.Color.Black;
            this.lblContadorTotalGeral.Location = new System.Drawing.Point(128, 95);
            this.lblContadorTotalGeral.Name = "lblContadorTotalGeral";
            this.lblContadorTotalGeral.Size = new System.Drawing.Size(21, 13);
            this.lblContadorTotalGeral.TabIndex = 6;
            this.lblContadorTotalGeral.Text = "R$";
            // 
            // pnlTotalizadores
            // 
            this.pnlTotalizadores.BackColor = System.Drawing.Color.LightSkyBlue;
            this.pnlTotalizadores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalizadores.Controls.Add(this.pgcTotalizadoresAvisos);
            this.pnlTotalizadores.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlTotalizadores.Location = new System.Drawing.Point(716, 173);
            this.pnlTotalizadores.Name = "pnlTotalizadores";
            this.pnlTotalizadores.Size = new System.Drawing.Size(246, 368);
            this.pnlTotalizadores.TabIndex = 3;
            // 
            // pgcTotalizadoresAvisos
            // 
            this.pgcTotalizadoresAvisos.Alignment = System.Windows.Forms.TabAlignment.Right;
            this.pgcTotalizadoresAvisos.Controls.Add(this.tabTotalizadores);
            this.pgcTotalizadoresAvisos.Controls.Add(this.tabLembretes);
            this.pgcTotalizadoresAvisos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgcTotalizadoresAvisos.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pgcTotalizadoresAvisos.Location = new System.Drawing.Point(0, 0);
            this.pgcTotalizadoresAvisos.Multiline = true;
            this.pgcTotalizadoresAvisos.Name = "pgcTotalizadoresAvisos";
            this.pgcTotalizadoresAvisos.SelectedIndex = 0;
            this.pgcTotalizadoresAvisos.Size = new System.Drawing.Size(244, 366);
            this.pgcTotalizadoresAvisos.SizeMode = System.Windows.Forms.TabSizeMode.FillToRight;
            this.pgcTotalizadoresAvisos.TabIndex = 0;
            // 
            // tabTotalizadores
            // 
            this.tabTotalizadores.BackColor = System.Drawing.Color.PowderBlue;
            this.tabTotalizadores.Controls.Add(this.label1);
            this.tabTotalizadores.Controls.Add(this.btnTotalizar);
            this.tabTotalizadores.Controls.Add(this.lblTotalizadores);
            this.tabTotalizadores.Controls.Add(this.lblContadorTotalGeral);
            this.tabTotalizadores.Controls.Add(this.lblTotalDebito);
            this.tabTotalizadores.Controls.Add(this.lblTotalGeral);
            this.tabTotalizadores.Controls.Add(this.lblContadorDebito);
            this.tabTotalizadores.Controls.Add(this.lblTotalCredito);
            this.tabTotalizadores.Controls.Add(this.lblContadorCredito);
            this.tabTotalizadores.Location = new System.Drawing.Point(4, 4);
            this.tabTotalizadores.Name = "tabTotalizadores";
            this.tabTotalizadores.Padding = new System.Windows.Forms.Padding(3);
            this.tabTotalizadores.Size = new System.Drawing.Size(217, 358);
            this.tabTotalizadores.TabIndex = 0;
            this.tabTotalizadores.Text = "Totalizadores";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Transparent;
            this.label1.Location = new System.Drawing.Point(9, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(441, 13);
            this.label1.TabIndex = 8;
            this.label1.Text = "--------------------------------------------------------------";
            // 
            // btnTotalizar
            // 
            this.btnTotalizar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnTotalizar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnTotalizar.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnTotalizar.ForeColor = System.Drawing.Color.Black;
            this.btnTotalizar.Image = ((System.Drawing.Image)(resources.GetObject("btnTotalizar.Image")));
            this.btnTotalizar.Location = new System.Drawing.Point(101, 113);
            this.btnTotalizar.Name = "btnTotalizar";
            this.btnTotalizar.Size = new System.Drawing.Size(98, 25);
            this.btnTotalizar.TabIndex = 7;
            this.btnTotalizar.Text = "Totalizar";
            this.btnTotalizar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnTotalizar.UseVisualStyleBackColor = false;
            this.btnTotalizar.Click += new System.EventHandler(this.btnTotalizar_Click);
            // 
            // tabLembretes
            // 
            this.tabLembretes.BackColor = System.Drawing.Color.PowderBlue;
            this.tabLembretes.Controls.Add(this.gridLembretes);
            this.tabLembretes.Location = new System.Drawing.Point(4, 4);
            this.tabLembretes.Name = "tabLembretes";
            this.tabLembretes.Padding = new System.Windows.Forms.Padding(3);
            this.tabLembretes.Size = new System.Drawing.Size(217, 358);
            this.tabLembretes.TabIndex = 1;
            this.tabLembretes.Text = "Lembretes";
            // 
            // gridLembretes
            // 
            this.gridLembretes.BackgroundColor = System.Drawing.Color.WhiteSmoke;
            this.gridLembretes.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridLembretes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.gridLembretes.Location = new System.Drawing.Point(3, 3);
            this.gridLembretes.Name = "gridLembretes";
            this.gridLembretes.Size = new System.Drawing.Size(211, 352);
            this.gridLembretes.TabIndex = 0;
            // 
            // pnlDescricaoLancamento
            // 
            this.pnlDescricaoLancamento.BackColor = System.Drawing.Color.PowderBlue;
            this.pnlDescricaoLancamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDescricaoLancamento.Controls.Add(this.lblLancamento);
            this.pnlDescricaoLancamento.Controls.Add(this.tsMenuPrincipal);
            this.pnlDescricaoLancamento.Controls.Add(this.edtData);
            this.pnlDescricaoLancamento.Controls.Add(this.lblDataLancamento);
            this.pnlDescricaoLancamento.Controls.Add(this.btnDeletar);
            this.pnlDescricaoLancamento.Controls.Add(this.btnAlterar);
            this.pnlDescricaoLancamento.Controls.Add(this.btnAdicionar);
            this.pnlDescricaoLancamento.Controls.Add(this.cbTipoCredito);
            this.pnlDescricaoLancamento.Controls.Add(this.lblTipoLancamento);
            this.pnlDescricaoLancamento.Controls.Add(this.edtValorLancamento);
            this.pnlDescricaoLancamento.Controls.Add(this.lblValor);
            this.pnlDescricaoLancamento.Controls.Add(this.edtLancamento);
            this.pnlDescricaoLancamento.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlDescricaoLancamento.Location = new System.Drawing.Point(3, 3);
            this.pnlDescricaoLancamento.Name = "pnlDescricaoLancamento";
            this.pnlDescricaoLancamento.Size = new System.Drawing.Size(707, 164);
            this.pnlDescricaoLancamento.TabIndex = 4;
            // 
            // lblLancamento
            // 
            this.lblLancamento.AutoSize = true;
            this.lblLancamento.BackColor = System.Drawing.Color.Transparent;
            this.lblLancamento.Font = new System.Drawing.Font("Hack", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLancamento.Location = new System.Drawing.Point(48, 8);
            this.lblLancamento.Name = "lblLancamento";
            this.lblLancamento.Size = new System.Drawing.Size(84, 14);
            this.lblLancamento.TabIndex = 0;
            this.lblLancamento.Text = "Lançamento:";
            // 
            // tsMenuPrincipal
            // 
            this.tsMenuPrincipal.BackColor = System.Drawing.Color.GhostWhite;
            this.tsMenuPrincipal.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Center;
            this.tsMenuPrincipal.CanOverflow = false;
            this.tsMenuPrincipal.Dock = System.Windows.Forms.DockStyle.Left;
            this.tsMenuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnAgenda,
            this.btnConfiguracoes});
            this.tsMenuPrincipal.LayoutStyle = System.Windows.Forms.ToolStripLayoutStyle.Table;
            this.tsMenuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.tsMenuPrincipal.Name = "tsMenuPrincipal";
            this.tsMenuPrincipal.ShowItemToolTips = false;
            this.tsMenuPrincipal.Size = new System.Drawing.Size(33, 162);
            this.tsMenuPrincipal.TabIndex = 9;
            // 
            // btnAgenda
            // 
            this.btnAgenda.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnAgenda.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNovaAgenda,
            this.btnAbrirAgenda,
            this.btnSalvarAgenda,
            this.btnFecharAgendar});
            this.btnAgenda.Image = ((System.Drawing.Image)(resources.GetObject("btnAgenda.Image")));
            this.btnAgenda.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnAgenda.Name = "btnAgenda";
            this.btnAgenda.Overflow = System.Windows.Forms.ToolStripItemOverflow.Never;
            this.btnAgenda.Size = new System.Drawing.Size(32, 20);
            // 
            // btnNovaAgenda
            // 
            this.btnNovaAgenda.Image = ((System.Drawing.Image)(resources.GetObject("btnNovaAgenda.Image")));
            this.btnNovaAgenda.Name = "btnNovaAgenda";
            this.btnNovaAgenda.Size = new System.Drawing.Size(243, 22);
            this.btnNovaAgenda.Text = "Nova Agenda de Lançamentos";
            this.btnNovaAgenda.Click += new System.EventHandler(this.btnNovaAgenda_Click);
            // 
            // btnAbrirAgenda
            // 
            this.btnAbrirAgenda.Image = ((System.Drawing.Image)(resources.GetObject("btnAbrirAgenda.Image")));
            this.btnAbrirAgenda.Name = "btnAbrirAgenda";
            this.btnAbrirAgenda.Size = new System.Drawing.Size(243, 22);
            this.btnAbrirAgenda.Text = "Abrir Agenda de Lançamentos";
            this.btnAbrirAgenda.Click += new System.EventHandler(this.btnAbrirAgenda_Click);
            // 
            // btnSalvarAgenda
            // 
            this.btnSalvarAgenda.Image = ((System.Drawing.Image)(resources.GetObject("btnSalvarAgenda.Image")));
            this.btnSalvarAgenda.Name = "btnSalvarAgenda";
            this.btnSalvarAgenda.Size = new System.Drawing.Size(243, 22);
            this.btnSalvarAgenda.Text = "Salvar Agenda de Lançamentos";
            this.btnSalvarAgenda.Click += new System.EventHandler(this.btnSalvarAgenda_Click);
            // 
            // btnFecharAgendar
            // 
            this.btnFecharAgendar.Image = ((System.Drawing.Image)(resources.GetObject("btnFecharAgendar.Image")));
            this.btnFecharAgendar.Name = "btnFecharAgendar";
            this.btnFecharAgendar.Size = new System.Drawing.Size(243, 22);
            this.btnFecharAgendar.Text = "Fechar Agenda de Lançamentos";
            this.btnFecharAgendar.Click += new System.EventHandler(this.btnFecharAgendar_Click);
            // 
            // btnConfiguracoes
            // 
            this.btnConfiguracoes.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.btnConfiguracoes.Image = ((System.Drawing.Image)(resources.GetObject("btnConfiguracoes.Image")));
            this.btnConfiguracoes.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnConfiguracoes.Name = "btnConfiguracoes";
            this.btnConfiguracoes.Size = new System.Drawing.Size(23, 20);
            this.btnConfiguracoes.Text = "toolStripButton1";
            this.btnConfiguracoes.Click += new System.EventHandler(this.btnConfiguracoes_Click);
            // 
            // edtData
            // 
            this.edtData.CalendarMonthBackground = System.Drawing.Color.WhiteSmoke;
            this.edtData.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.edtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.edtData.Location = new System.Drawing.Point(555, 5);
            this.edtData.Name = "edtData";
            this.edtData.Size = new System.Drawing.Size(121, 20);
            this.edtData.TabIndex = 2;
            // 
            // lblDataLancamento
            // 
            this.lblDataLancamento.AutoSize = true;
            this.lblDataLancamento.Font = new System.Drawing.Font("Hack", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataLancamento.Location = new System.Drawing.Point(416, 8);
            this.lblDataLancamento.Name = "lblDataLancamento";
            this.lblDataLancamento.Size = new System.Drawing.Size(140, 14);
            this.lblDataLancamento.TabIndex = 10;
            this.lblDataLancamento.Text = "Data de Vencimento:";
            // 
            // btnDeletar
            // 
            this.btnDeletar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnDeletar.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.btnDeletar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDeletar.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDeletar.Image = ((System.Drawing.Image)(resources.GetObject("btnDeletar.Image")));
            this.btnDeletar.Location = new System.Drawing.Point(464, 57);
            this.btnDeletar.Name = "btnDeletar";
            this.btnDeletar.Size = new System.Drawing.Size(98, 25);
            this.btnDeletar.TabIndex = 7;
            this.btnDeletar.Text = "Deletar";
            this.btnDeletar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnDeletar.UseVisualStyleBackColor = false;
            this.btnDeletar.Click += new System.EventHandler(this.btnDeletar_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAlterar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAlterar.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAlterar.Image = ((System.Drawing.Image)(resources.GetObject("btnAlterar.Image")));
            this.btnAlterar.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.btnAlterar.Location = new System.Drawing.Point(578, 57);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(98, 25);
            this.btnAlterar.TabIndex = 8;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.btnAlterar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAlterar.UseVisualStyleBackColor = false;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnAdicionar
            // 
            this.btnAdicionar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnAdicionar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnAdicionar.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAdicionar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAdicionar.Image = ((System.Drawing.Image)(resources.GetObject("btnAdicionar.Image")));
            this.btnAdicionar.Location = new System.Drawing.Point(350, 57);
            this.btnAdicionar.Name = "btnAdicionar";
            this.btnAdicionar.Size = new System.Drawing.Size(98, 25);
            this.btnAdicionar.TabIndex = 5;
            this.btnAdicionar.Text = "Adicionar";
            this.btnAdicionar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnAdicionar.UseVisualStyleBackColor = false;
            this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
            // 
            // cbTipoCredito
            // 
            this.cbTipoCredito.BackColor = System.Drawing.Color.WhiteSmoke;
            this.cbTipoCredito.FormattingEnabled = true;
            this.cbTipoCredito.Items.AddRange(new object[] {
            "Crédito",
            "Débito"});
            this.cbTipoCredito.Location = new System.Drawing.Point(555, 30);
            this.cbTipoCredito.Name = "cbTipoCredito";
            this.cbTipoCredito.Size = new System.Drawing.Size(121, 21);
            this.cbTipoCredito.TabIndex = 4;
            // 
            // lblTipoLancamento
            // 
            this.lblTipoLancamento.AutoSize = true;
            this.lblTipoLancamento.Font = new System.Drawing.Font("Hack", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoLancamento.Location = new System.Drawing.Point(416, 33);
            this.lblTipoLancamento.Name = "lblTipoLancamento";
            this.lblTipoLancamento.Size = new System.Drawing.Size(140, 14);
            this.lblTipoLancamento.TabIndex = 4;
            this.lblTipoLancamento.Text = "Tipo do Lançamento:";
            // 
            // edtValorLancamento
            // 
            this.edtValorLancamento.BackColor = System.Drawing.Color.WhiteSmoke;
            this.edtValorLancamento.Location = new System.Drawing.Point(132, 30);
            this.edtValorLancamento.Name = "edtValorLancamento";
            this.edtValorLancamento.Size = new System.Drawing.Size(121, 20);
            this.edtValorLancamento.TabIndex = 3;
            this.edtValorLancamento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edtValorLancamento_KeyPress);
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Font = new System.Drawing.Font("Hack", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValor.Location = new System.Drawing.Point(48, 33);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(84, 14);
            this.lblValor.TabIndex = 2;
            this.lblValor.Text = "Valor (R$):";
            // 
            // edtLancamento
            // 
            this.edtLancamento.BackColor = System.Drawing.Color.WhiteSmoke;
            this.edtLancamento.Location = new System.Drawing.Point(132, 5);
            this.edtLancamento.Name = "edtLancamento";
            this.edtLancamento.Size = new System.Drawing.Size(254, 20);
            this.edtLancamento.TabIndex = 1;
            // 
            // calendarioPrincipal
            // 
            this.calendarioPrincipal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.calendarioPrincipal.Location = new System.Drawing.Point(0, 0);
            this.calendarioPrincipal.Name = "calendarioPrincipal";
            this.calendarioPrincipal.TabIndex = 8;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 73.92704F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.07296F));
            this.tableLayoutPanel1.Controls.Add(this.pnlDescricaoLancamento, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.pnlGridLancamentos, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.pnlTotalizadores, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.pnlCalendario, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 31.43382F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 68.56618F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(965, 544);
            this.tableLayoutPanel1.TabIndex = 10;
            // 
            // pnlCalendario
            // 
            this.pnlCalendario.Controls.Add(this.calendarioPrincipal);
            this.pnlCalendario.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlCalendario.Location = new System.Drawing.Point(716, 3);
            this.pnlCalendario.Name = "pnlCalendario";
            this.pnlCalendario.Size = new System.Drawing.Size(246, 164);
            this.pnlCalendario.TabIndex = 11;
            // 
            // OrganizerAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.GhostWhite;
            this.ClientSize = new System.Drawing.Size(965, 544);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "OrganizerAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Organizer Account v0.03";
            this.pnlGridLancamentos.ResumeLayout(false);
            this.pnlGridLancamentos.PerformLayout();
            this.tsFuncionalidades.ResumeLayout(false);
            this.tsFuncionalidades.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLancamentos)).EndInit();
            this.pnlTotalizadores.ResumeLayout(false);
            this.pgcTotalizadoresAvisos.ResumeLayout(false);
            this.tabTotalizadores.ResumeLayout(false);
            this.tabTotalizadores.PerformLayout();
            this.tabLembretes.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridLembretes)).EndInit();
            this.pnlDescricaoLancamento.ResumeLayout(false);
            this.pnlDescricaoLancamento.PerformLayout();
            this.tsMenuPrincipal.ResumeLayout(false);
            this.tsMenuPrincipal.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.pnlCalendario.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.Panel pnlGridLancamentos;
        private System.Windows.Forms.Label lblTotalizadores;
        private System.Windows.Forms.Label lblTotalCredito;
        private System.Windows.Forms.Label lblTotalDebito;
        private System.Windows.Forms.Label lblTotalGeral;
        private System.Windows.Forms.Label lblContadorCredito;
        private System.Windows.Forms.Label lblContadorDebito;
        private System.Windows.Forms.Label lblContadorTotalGeral;
        private System.Windows.Forms.Panel pnlTotalizadores;
        private System.Windows.Forms.Panel pnlDescricaoLancamento;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.TextBox edtLancamento;
        private System.Windows.Forms.Label lblLancamento;
        private System.Windows.Forms.TextBox edtValorLancamento;
        private System.Windows.Forms.Label lblTipoLancamento;
        private System.Windows.Forms.ComboBox cbTipoCredito;
        private System.Windows.Forms.Button btnDeletar;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnAdicionar;
        private System.Windows.Forms.Label lblDataLancamento;
        private System.Windows.Forms.SaveFileDialog saveFileOrganizerAccount;
        private System.Windows.Forms.DataGridView gridLancamentos;
        private System.Windows.Forms.OpenFileDialog openFileOrganizerAccount;
        private System.Windows.Forms.DateTimePicker edtData;
        private System.Windows.Forms.Button btnTotalizar;
        private System.Windows.Forms.MonthCalendar calendarioPrincipal;
        private System.Windows.Forms.TabControl pgcTotalizadoresAvisos;
        private System.Windows.Forms.TabPage tabTotalizadores;
        private System.Windows.Forms.TabPage tabLembretes;
        private System.Windows.Forms.ToolStrip tsMenuPrincipal;
        private System.Windows.Forms.ToolStripSplitButton btnAgenda;
        private System.Windows.Forms.ToolStripMenuItem btnNovaAgenda;
        private System.Windows.Forms.ToolStripMenuItem btnAbrirAgenda;
        private System.Windows.Forms.ToolStripMenuItem btnFecharAgendar;
        private System.Windows.Forms.ToolStripMenuItem btnSalvarAgenda;
        private System.Windows.Forms.ToolStripButton btnConfiguracoes;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel pnlCalendario;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView gridLembretes;
        private System.Windows.Forms.ToolStrip tsFuncionalidades;
        private System.Windows.Forms.ToolStripButton btnPesquisarLancamento;
    }
}