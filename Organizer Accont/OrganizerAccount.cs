﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Organizer_Accont
{
    public partial class OrganizerAccount : Form
    {
        string DiretorioXMLAtual, diretorioPadrao, usuarioPadrao = "";
        Lancamentos LancamentosAtual = null;
        

        public OrganizerAccount()
        {
            string diretorioConfiguracoes = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\Organizer Account";
            InitializeComponent();
            this.diretorioPadrao = Geral.RetornarDiretorioPadrao(diretorioConfiguracoes);
            this.usuarioPadrao   = Geral.RetornarUsuarioPadrao(diretorioConfiguracoes);
        }


        #region Acoes Componentes

        private void btnAdicionar_Click(object sender, EventArgs e)
        {
            this.AdicionarLancamento();
        }

        private void btnAbrir_Click(object sender, EventArgs e)
        {
            this.AbrirArquivo();
            if (this.DiretorioXMLAtual != null)
            {
                this.TotalizarValores();
            }
        }

        private void btnFechar_Click(object sender, EventArgs e)
        {
            this.FecharArquivo();
        }

        private void btnSalvar_Click(object sender, EventArgs e)
        {
            Geral.SalvarXML(this.DiretorioXMLAtual, LancamentosAtual);
        }

        private void btnNovo_Click(object sender, EventArgs e)
        {
            this.NovoArquivo();
        }

        private void edtValorLancamento_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != ','))
            {
                e.Handled = true;
            }
            if ((e.KeyChar == ',') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void btnConfiguracoes_Click(object sender, EventArgs e)
        {
            FormAuxiliar formAuxiliar = new FormAuxiliar();
            formAuxiliar.StartPosition = FormStartPosition.CenterParent;
            formAuxiliar.ShowDialog(this);
        }

        private void AssociarDataSourceGrid()
        {
            if (DiretorioXMLAtual != "")
            {
                gridLancamentos.DataSource = Geral.LerXML(DiretorioXMLAtual).Lancamento;
                this.SetarConiguracaoVisualGridLancamentos();
            }
            else
            {
                gridLancamentos.DataSource = null;
            }
        }

        private void btnAlterar_Click(object sender, EventArgs e)
        {
            this.AlterarLancamento();
        }

        private void btnDeletar_Click(object sender, EventArgs e)
        {
            this.DeletarLancamento();
        }

        private void btnTotalizar_Click(object sender, EventArgs e)
        {
            this.TotalizarValores();
        }

        #endregion

        #region Funcoes com o arquivo
        private void NovoArquivo()
        {
            this.LimparLayout();
            this.LancamentosAtual = Geral.CriarXML(this.RetornarDiretorioSalvar());

            this.AssociarDataSourceGrid();
        }

        private void FecharArquivo()
        {
            this.LancamentosAtual = null;
            this.DiretorioXMLAtual = "";
            this.AssociarDataSourceGrid();
            this.LimparLayout();
        }

        private void AbrirArquivo()
        {
            openFileOrganizerAccount.InitialDirectory = this.diretorioPadrao;
            openFileOrganizerAccount.AddExtension = true;
            openFileOrganizerAccount.DefaultExt = ".xml";
            openFileOrganizerAccount.ShowDialog(this);

            if (openFileOrganizerAccount.FileName != "")
            {
                this.LancamentosAtual = Geral.LerXML(openFileOrganizerAccount.FileName);
                this.DiretorioXMLAtual = openFileOrganizerAccount.FileName;
                this.AssociarDataSourceGrid();
            }
        }

        #endregion

        #region Funcoes do layout

        private void LimparLayout()
        {
            gridLancamentos.DataSource = null;
            this.LimparCampos();
            this.LimparTotalizadores();
        }

        private void LimparTotalizadores()
        {
            lblContadorDebito.Text = lblContadorCredito.Text = lblContadorTotalGeral.Text = "R$";
        }
         
        private void LimparCampos()
        {
            edtData.Text = edtLancamento.Text = edtValorLancamento.Text = string.Empty;
            cbTipoCredito.ResetText();
        }

        private void SetarConiguracaoVisualGridLancamentos()
        {
            gridLancamentos.Columns["usuario"].HeaderText        = "Usuário";
            gridLancamentos.Columns["descricao"].HeaderText      = "Descrição do Lançamentos";
            gridLancamentos.Columns["valor"].HeaderText          = "Valor(R$)";
            gridLancamentos.Columns["dataVencimento"].HeaderText = "Data de Vencimento";
            gridLancamentos.Columns["tipoLancamento"].HeaderText = "Tipo do Lançamento";

            gridLancamentos.Columns["descricao"].DisplayIndex       = 0;
            gridLancamentos.Columns["valor"].DisplayIndex           = 1;
            gridLancamentos.Columns["tipoLancamento"].DisplayIndex  = 2;
            gridLancamentos.Columns["dataVencimento"].DisplayIndex  = 3;
            gridLancamentos.Columns["usuario"].DisplayIndex         = 4;

            gridLancamentos.Columns["ID"].Visible = false;
        }

        private bool ValidacoesAdicionar()
        {
            return ((edtData.Text != "") && (edtLancamento.Text != "") && (edtValorLancamento.Text != "") && (cbTipoCredito.Text != "") && (this.DiretorioXMLAtual != "") && (this.LancamentosAtual != null));        }

        private void VincularLancamentoNoLayout(Lancamento lancamentoVincular)
        {
            if (lancamentoVincular != null)
            {
                edtLancamento.Text      = lancamentoVincular.descricao.ToString();
                edtValorLancamento.Text = lancamentoVincular.valor.ToString();
                edtData.Text            = lancamentoVincular.dataVencimento.ToShortDateString();
                cbTipoCredito.Text      = lancamentoVincular.tipoLancamento.ToString();
            }
        }

        private void DefineLayout(string operacao)
        {
            if (operacao == "alterar")
            {
                btnAdicionar.Visible = false;
                btnDeletar.Visible   = false;
                btnAlterar.Text      = "Salvar";
            }
            else if (operacao == "default")
            {
                btnAdicionar.Visible = true;
                btnDeletar.Visible   = true;
                btnAlterar.Text      = "Alterar";
            }
        }

        #endregion

        #region Funcoes Gerais
        private void AdicionarLancamento()
        {
            if (this.ValidacoesAdicionar())
            {
                Lancamento lancamentoAdicionar = new Lancamento();

                if (this.LancamentosAtual.Lancamento.Count > 0)
                {
                    lancamentoAdicionar.ID         = this.LancamentosAtual.Lancamento[this.LancamentosAtual.Lancamento.Count - 1].ID + 1;
                }
                else
                {
                    lancamentoAdicionar.ID         = 1;
                }
                lancamentoAdicionar.descricao      = edtLancamento.Text.ToString();
                lancamentoAdicionar.valor          = double.Parse(edtValorLancamento.Text);
                lancamentoAdicionar.dataVencimento = DateTime.Parse(edtData.Text);
                lancamentoAdicionar.tipoLancamento = cbTipoCredito.Text.ToString();
                lancamentoAdicionar.usuario        = this.usuarioPadrao;

                this.LancamentosAtual.Lancamento.Add(lancamentoAdicionar);
                Geral.SalvarXML(DiretorioXMLAtual, LancamentosAtual);

                this.AssociarDataSourceGrid();
                this.LimparCampos();
                this.TotalizarValores();
            }
        }

        private string RetornarDiretorioSalvar()
        {
            saveFileOrganizerAccount.InitialDirectory = diretorioPadrao;
            saveFileOrganizerAccount.AddExtension = true;
            saveFileOrganizerAccount.DefaultExt = ".xml";
            saveFileOrganizerAccount.ShowDialog(this);
            this.DiretorioXMLAtual = saveFileOrganizerAccount.FileName;

            return DiretorioXMLAtual;
        }

        private void DeletarLancamento()
        {
            try
            {
                Lancamento lancamentoDeletar = (Lancamento)gridLancamentos.CurrentRow.DataBoundItem;
                LancamentosAtual.Lancamento.Remove(LancamentosAtual.Lancamento.Find(l => l.ID == lancamentoDeletar.ID));
                Geral.SalvarXML(this.DiretorioXMLAtual, this.LancamentosAtual);
                this.AssociarDataSourceGrid();
                this.TotalizarValores();
            }
            catch (Exception)
            {
                MessageBox.Show("É necessário ter um lançamento selecionado para alterar", "Organizer Account", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }

        private void AlterarLancamento()
        {
            try
            {
                Lancamento lancamentoSelecionado = (Lancamento)gridLancamentos.CurrentRow.DataBoundItem;
                if (lancamentoSelecionado != null)
                {
                    if (btnAlterar.Text == "Alterar")
                    {
                        this.VincularLancamentoNoLayout(lancamentoSelecionado);
                        this.DefineLayout("alterar");
                    }
                    else if (btnAlterar.Text == "Salvar")
                    {
                        Lancamento lancamentoAlterar = this.LancamentosAtual.Lancamento.Find(l => l.ID == lancamentoSelecionado.ID);

                        lancamentoAlterar.descricao = edtLancamento.Text.ToString();
                        lancamentoAlterar.valor = double.Parse(edtValorLancamento.Text);
                        lancamentoAlterar.dataVencimento = DateTime.Parse(edtData.Text);
                        lancamentoAlterar.tipoLancamento = cbTipoCredito.Text.ToString();

                        Geral.SalvarXML(this.DiretorioXMLAtual, this.LancamentosAtual);
                        this.DefineLayout("default");
                        this.AssociarDataSourceGrid();
                        this.TotalizarValores();
                    }
                }
            }
            catch (Exception)
            {
                MessageBox.Show("É necessário ter um lançamento selecionado para alterar", "Organizer Account", MessageBoxButtons.OK,MessageBoxIcon.Exclamation);
            }
        }

        private void TotalizarValores()
        {
            try
            {
                double totalCredito, totalDebito, totalGeral;
                totalCredito = totalDebito = totalGeral = 0;
                foreach (Lancamento l in this.LancamentosAtual)
                {
                    if (l.tipoLancamento == "Crédito")
                    {
                        totalCredito += l.valor;
                    }
                    else if (l.tipoLancamento == "Débito")
                    {
                        totalDebito += l.valor;
                    }
                }
                totalGeral = totalCredito - totalDebito;

                if (totalGeral > 0)
                {
                    lblContadorTotalGeral.ForeColor = Color.Green;
                }
                else if (totalGeral < 0)
                {
                    lblContadorTotalGeral.ForeColor = Color.Red;
                }

                lblContadorCredito.Text = "R$" + totalCredito.ToString();
                lblContadorDebito.Text = "R$" + totalDebito.ToString();
                lblContadorTotalGeral.Text = "R$" + totalGeral.ToString();
            }
            catch (Exception)
            {
                MessageBox.Show("É necessário ter um arquivo carregado para totalizar valores", "Organizer Account", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }
            
        }
        #endregion
    }
}
