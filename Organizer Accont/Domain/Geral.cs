﻿using System;
using System.Xml.Linq;

namespace Organizer_Account
{
    public abstract class Geral
    {
        public static string RetornarDiretorioPadrao(string diretorioConfiguracoes)
        {
            try
            {
                ConfiguracoesPadrao configuracoesPadrao = new ConfiguracoesPadrao();

                XElement xmlLoad = XElement.Load(diretorioConfiguracoes + @"\ConfiguracoesPadrao.xml");
                configuracoesPadrao = ServiceSerializer.ServiceSerializer.Deserializer<ConfiguracoesPadrao>(xmlLoad);

                return configuracoesPadrao.DiretorioPadrao;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public static double RetornarDiasAviso(string diretorioConfiguracoes)
        {
            try
            {
                ConfiguracoesPadrao configuracoesPadrao = new ConfiguracoesPadrao();

                XElement xmlLoad = XElement.Load(diretorioConfiguracoes + @"\ConfiguracoesPadrao.xml");
                configuracoesPadrao = ServiceSerializer.ServiceSerializer.Deserializer<ConfiguracoesPadrao>(xmlLoad);

                return configuracoesPadrao.diasAviso;
            }
            catch (Exception)
            {
                return -1;
            }
        }

        public static void SalvarXML(string diretorioSalvar, Lancamentos lancamentosSalvar)
        {
            if ((diretorioSalvar != "") && (lancamentosSalvar != null))
            {
                XElement xmlRetornoSalvar = ServiceSerializer.ServiceSerializer.Serializer<Lancamentos>(lancamentosSalvar);
                xmlRetornoSalvar.Save(diretorioSalvar);
            }
        }

        public static Lancamentos CriarXML(string diretorioSalvar)
        {
            if (diretorioSalvar != "")
            {
                Lancamentos lancamentos = new Lancamentos();
                XElement xmlRetornoSalvar = ServiceSerializer.ServiceSerializer.Serializer<Lancamentos>(lancamentos);
                xmlRetornoSalvar.Save(diretorioSalvar);

                return ServiceSerializer.ServiceSerializer.Deserializer<Lancamentos>(xmlRetornoSalvar);
            }
            else
            {
                return null;
            }
        }

        public static Lancamentos LerXML(string diretorioXML)
        {
            Lancamentos lancamentosXML = null;
            if (diretorioXML != "")
            {
                XElement xmlRetornoLer = XElement.Load(diretorioXML);
                lancamentosXML = ServiceSerializer.ServiceSerializer.Deserializer<Lancamentos>(xmlRetornoLer);
                return lancamentosXML;
            }
            else
            {
                return lancamentosXML;
            }
        }

        public static String RetornarUsuarioPadrao(string diretorioAplicacao)
        {
            try
            {
                string diretorioConfiguracoesPadrao = diretorioAplicacao + @"\ConfiguracoesPadrao.xml";
                ConfiguracoesPadrao configuracoesPadrao = new ConfiguracoesPadrao();

                XElement xmlLoad = XElement.Load(diretorioAplicacao + @"\ConfiguracoesPadrao.xml");
                configuracoesPadrao = ServiceSerializer.ServiceSerializer.Deserializer<ConfiguracoesPadrao>(xmlLoad);

                return configuracoesPadrao.usuarioPadrao;
            }
            catch (Exception)
            {
                return "";
            }
        }
    }
}