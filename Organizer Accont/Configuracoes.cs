﻿using System;
using System.IO;
using System.Windows.Forms;
using System.Xml.Linq;

namespace Organizer_Account
{
    public partial class FormConfiguracoes : Form
    {
        //Declaração das variaveis globais
        private string diretorioPadrao = "";

        private string diretorioConfiguracoes = Environment.GetFolderPath(Environment.SpecialFolder.Personal) + @"\Organizer Account";

        public FormConfiguracoes()
        {
            InitializeComponent();
            this.CarregarConfiguracoesExistente();
        }

        private void CarregarConfiguracoesExistente()
        {
            if (File.Exists(diretorioConfiguracoes + @"\ConfiguracoesPadrao.xml"))
            {
                XElement xmlLoad = XElement.Load(diretorioConfiguracoes + @"\ConfiguracoesPadrao.xml");
                ConfiguracoesPadrao configuracoesCarregadas = ServiceSerializer.ServiceSerializer.Deserializer<ConfiguracoesPadrao>(xmlLoad);
                edtUsuarioPadrao.Text = configuracoesCarregadas.usuarioPadrao.ToString();
                edtDiasAlerta.Value = configuracoesCarregadas.diasAviso;
            }
        }

        private void btnDiretorioPadrao_Click(object sender, EventArgs e)
        {
            fdlDiretorioPadrao.ShowDialog();
            if (fdlDiretorioPadrao.SelectedPath != "")
            {
                this.diretorioPadrao = fdlDiretorioPadrao.SelectedPath;
            }
        }

        private void FormAuxiliar_FormClosed(object sender, FormClosedEventArgs e)
        {
            this.SalvarConfiguracoesPadrao();
        }

        private void SalvarConfiguracoesPadrao()
        {
            ConfiguracoesPadrao configuracoesPadrao = new ConfiguracoesPadrao();

            if (!Directory.Exists(diretorioConfiguracoes))
            {
                Directory.CreateDirectory(diretorioConfiguracoes);
            }

            if (this.diretorioPadrao != "")
            {
                configuracoesPadrao.DiretorioPadrao = this.diretorioPadrao;
            }
            configuracoesPadrao.usuarioPadrao = this.edtUsuarioPadrao.Text;
            configuracoesPadrao.diasAviso = (int)this.edtDiasAlerta.Value;

            XElement xmlReturnSave = ServiceSerializer.ServiceSerializer.Serializer<ConfiguracoesPadrao>(configuracoesPadrao);
            xmlReturnSave.Save(diretorioConfiguracoes + @"\ConfiguracoesPadrao.xml");
        }
    }
}