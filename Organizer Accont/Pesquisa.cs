﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Windows.Forms;

namespace Organizer_Account
{
    public partial class Pesquisa : Form
    {
        private Lancamentos lancamentosAtual;
        private List<Lancamento> gLancamentosResultados;

        public Pesquisa(Lancamentos lancamentosAtual)
        {
            InitializeComponent();
            this.SetarLayoutPesquisa();
            this.lancamentosAtual = lancamentosAtual;
        }

        private void SetarLayoutPesquisa()
        {
            pgcFormPesquisa.Controls.Clear();
            pgcFormPesquisa.Controls.Remove(tabResultados);
            pgcFormPesquisa.Controls.Add(tabPesquisa);
        }

        private void SetarLayourResultado()
        {
            pgcFormPesquisa.Controls.Clear();
            pgcFormPesquisa.Controls.Remove(tabPesquisa);
            pgcFormPesquisa.Controls.Add(tabResultados);
        }

        private void btnPesquisar_Click(object sender, EventArgs e)
        {
            this.PesquisarLancamento();
        }

        private void PesquisarLancamento()
        {
            List<Lancamento> lancamentosResultados = new List<Lancamento>();

            if (chk100Compativel.Checked)
            {
                lancamentosResultados = this.PesquisarLancamentos100Compativel();
            }
            else
            {
                lancamentosResultados = this.PesquisarLancamentosAdaptavel();
            }

            if (lancamentosResultados.Count > 0)
            {
                this.gLancamentosResultados = lancamentosResultados;
                this.MostrarResultados();
                this.SetarLayourResultado();
            }
        }

        private List<Lancamento> PesquisarLancamentos100Compativel()
        {
            List<Lancamento> lancamentosEncontrados = new List<Lancamento>();

            if (edtLancamento.Text != "")
            {
                lancamentosEncontrados = lancamentosAtual.Lancamento.Where(l => l.descricao.Contains(edtLancamento.Text)).ToList<Lancamento>();
            }

            if (chkConsiderarData.Checked)
            {
                lancamentosEncontrados = lancamentosEncontrados.Where(l => l.dataVencimento.Date.ToShortDateString() == dtDataVencimento.Value.ToShortDateString()).ToList<Lancamento>();
            }

            if (cbTipoLancamento.SelectedItem != null)
            {
                lancamentosEncontrados = lancamentosEncontrados.Where(l => l.tipoLancamento == cbTipoLancamento.SelectedItem.ToString()).ToList<Lancamento>();
            }

            if (edtUsuario.Text != "")
            {
                lancamentosEncontrados = lancamentosEncontrados.Where(l => l.usuario.Contains(edtUsuario.Text)).ToList<Lancamento>();
            }

            return lancamentosEncontrados;
        }

        private List<Lancamento> PesquisarLancamentosAdaptavel()
        {
            List<Lancamento> lancamentosEncontrados = new List<Lancamento>();
            int contador = 1;

            lancamentosEncontrados = this.PesquisarLancamentos100Compativel();

            if (lancamentosEncontrados.Count == 0)
            {
                do
                {
                    switch (contador)
                    {
                        case 1:
                            {
                                if (edtLancamento.Text != "")
                                {
                                    lancamentosEncontrados = lancamentosAtual.Lancamento.Where(l => l.descricao.Contains(edtLancamento.Text)).ToList<Lancamento>();
                                }
                                break;
                            }

                        case 2:
                            {
                                if (chkConsiderarData.Checked)
                                {
                                    if (lancamentosEncontrados.Count > 0)
                                    {
                                        lancamentosEncontrados = lancamentosEncontrados.Where(l => l.dataVencimento.Date.ToLocalTime() == dtDataVencimento.Value.ToLocalTime()).ToList<Lancamento>();
                                    }
                                    else
                                    {
                                        lancamentosEncontrados = lancamentosAtual.Lancamento.Where(l => l.dataVencimento.Date.ToShortDateString() == dtDataVencimento.Value.ToShortDateString()).ToList<Lancamento>();
                                    }
                                }
                                break;
                            }

                        case 3:
                            {
                                if (cbTipoLancamento.SelectedItem != null)
                                {
                                    if (lancamentosEncontrados.Count > 0)
                                    {
                                        lancamentosEncontrados = lancamentosEncontrados.Where(l => l.tipoLancamento == cbTipoLancamento.SelectedItem.ToString()).ToList<Lancamento>();
                                    }
                                    else
                                    {
                                        lancamentosEncontrados = lancamentosAtual.Lancamento.Where(l => l.tipoLancamento == cbTipoLancamento.SelectedItem.ToString()).ToList<Lancamento>();
                                    }
                                }
                                break;
                            }
                        case 4:
                            {
                                if (edtUsuario.Text != "")
                                {
                                    if (lancamentosEncontrados.Count > 0)
                                    {
                                        lancamentosEncontrados = lancamentosEncontrados.Where(l => l.usuario.Contains(edtUsuario.Text)).ToList<Lancamento>();
                                    }
                                    else
                                    {
                                        lancamentosEncontrados = lancamentosAtual.Lancamento.Where(l => l.usuario.Contains(edtUsuario.Text)).ToList<Lancamento>();
                                    }
                                }
                                break;
                            }
                    }

                    contador++;
                } while (contador <= 4);
            }

            return lancamentosEncontrados;
        }

        private void MostrarResultados()
        {
            foreach (Lancamento l in gLancamentosResultados)
            {
                cbResultados.Items.Add(l.descricao);
            }
        }

        private void cbResultados_TextChanged(object sender, EventArgs e)
        {
            this.MostrarInformacoesLancamentoSelecionado(cbResultados.SelectedItem.ToString());
        }

        private void MostrarInformacoesLancamentoSelecionado(string descricaoLancamento)
        {
            if (descricaoLancamento != "")
            {
                Lancamento lancamentoSelecionado = gLancamentosResultados.Find(l => l.descricao == descricaoLancamento);

                edtValor.Text = lancamentoSelecionado.valor.ToString();
                dtDataVencimentoResultado.Value = lancamentoSelecionado.dataVencimento;
                edtTipoLancamentoResultado.Text = lancamentoSelecionado.tipoLancamento;
                edtUsuarioResultado.Text = lancamentoSelecionado.usuario;
            }
        }

        private void btnLimparPesquisa_Click(object sender, EventArgs e)
        {
            this.LimparPesquisa();
        }

        private void LimparPesquisa()
        {
            edtLancamento.ResetText();
            edtUsuario.ResetText();
            dtDataVencimento.ResetText();
            cbTipoLancamento.ResetText();
            edtUsuario.ResetText();
        }

        private void LimparResultados()
        {
            edtTipoLancamentoResultado.ResetText();
            edtUsuarioResultado.ResetText();
            cbResultados.Items.Clear();
            edtValor.ResetText();
            dtDataVencimentoResultado.ResetText();
        }

        private void btnVoltar_Click(object sender, EventArgs e)
        {
            this.SetarLayoutPesquisa();
            this.gLancamentosResultados.Clear();
            this.LimparPesquisa();
            this.LimparResultados();
        }
    }
}