﻿using System;
using System.Xml.Serialization;

namespace Organizer_Account
{
    [Serializable()]
    [XmlRoot("Configuracoes")]
    public class ConfiguracoesPadrao
    {
        [XmlElement("diretorioPadrao")]
        public string DiretorioPadrao;

        [XmlElement("usuarioPadrao")]
        public string usuarioPadrao;

        [XmlElement("diasAviso")]
        public int diasAviso;
    }
}