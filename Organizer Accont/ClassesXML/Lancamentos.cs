﻿using System;
using System.Collections.Generic;
using System.Xml.Serialization;

namespace Organizer_Account
{
    [Serializable()]
    [XmlRoot("Lancamentos")]
    public class Lancamentos
    {
        [XmlElement("Lancamento")]
        public List<Lancamento> Lancamento { get; set; }

        public IEnumerator<Lancamento> GetEnumerator()
        {
            return this.Lancamento.GetEnumerator();
        }
    }
}