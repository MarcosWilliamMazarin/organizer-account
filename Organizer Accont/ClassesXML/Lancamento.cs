﻿using System;
using System.Xml.Serialization;

namespace Organizer_Account
{
    [Serializable()]
    public class Lancamento
    {
        [XmlElement("ID")]
        public int ID { get; set; }

        [XmlElement("usuario")]
        public string usuario { get; set; }

        [XmlElement("descricao")]
        public string descricao { get; set; }

        [XmlElement("valor")]
        public double valor { get; set; }

        [XmlElement("dataVencimento")]
        public DateTime dataVencimento { get; set; }

        [XmlElement("tipoLancamento")]
        public string tipoLancamento { get; set; }
    }
}