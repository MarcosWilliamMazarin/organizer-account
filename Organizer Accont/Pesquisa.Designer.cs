﻿namespace Organizer_Account
{
    partial class Pesquisa
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Pesquisa));
            this.pgcFormPesquisa = new System.Windows.Forms.TabControl();
            this.tabPesquisa = new System.Windows.Forms.TabPage();
            this.gpConfiguracoesPesquisa = new System.Windows.Forms.GroupBox();
            this.chk100Compativel = new System.Windows.Forms.CheckBox();
            this.chkConsiderarData = new System.Windows.Forms.CheckBox();
            this.btnPesquisar = new System.Windows.Forms.Button();
            this.edtUsuario = new System.Windows.Forms.TextBox();
            this.cbTipoLancamento = new System.Windows.Forms.ComboBox();
            this.dtDataVencimento = new System.Windows.Forms.DateTimePicker();
            this.edtLancamento = new System.Windows.Forms.TextBox();
            this.lblUsuario = new System.Windows.Forms.Label();
            this.lblTipoLancamento = new System.Windows.Forms.Label();
            this.lblDataVencimento = new System.Windows.Forms.Label();
            this.lblLancamento = new System.Windows.Forms.Label();
            this.tabResultados = new System.Windows.Forms.TabPage();
            this.dtDataVencimentoResultado = new System.Windows.Forms.DateTimePicker();
            this.edtValor = new System.Windows.Forms.TextBox();
            this.edtUsuarioResultado = new System.Windows.Forms.TextBox();
            this.lblUsuarioResultado = new System.Windows.Forms.Label();
            this.lblTipoLancamentoResultado = new System.Windows.Forms.Label();
            this.lblDataVencimentoResultado = new System.Windows.Forms.Label();
            this.lblValorResultado = new System.Windows.Forms.Label();
            this.lblResultados = new System.Windows.Forms.Label();
            this.cbResultados = new System.Windows.Forms.ComboBox();
            this.edtTipoLancamentoResultado = new System.Windows.Forms.TextBox();
            this.btnLimparPesquisa = new System.Windows.Forms.Button();
            this.btnVoltar = new System.Windows.Forms.Button();
            this.pgcFormPesquisa.SuspendLayout();
            this.tabPesquisa.SuspendLayout();
            this.gpConfiguracoesPesquisa.SuspendLayout();
            this.tabResultados.SuspendLayout();
            this.SuspendLayout();
            // 
            // pgcFormPesquisa
            // 
            this.pgcFormPesquisa.Controls.Add(this.tabPesquisa);
            this.pgcFormPesquisa.Controls.Add(this.tabResultados);
            this.pgcFormPesquisa.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgcFormPesquisa.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pgcFormPesquisa.Location = new System.Drawing.Point(0, 0);
            this.pgcFormPesquisa.Name = "pgcFormPesquisa";
            this.pgcFormPesquisa.SelectedIndex = 0;
            this.pgcFormPesquisa.Size = new System.Drawing.Size(460, 238);
            this.pgcFormPesquisa.TabIndex = 0;
            // 
            // tabPesquisa
            // 
            this.tabPesquisa.BackColor = System.Drawing.Color.PowderBlue;
            this.tabPesquisa.Controls.Add(this.btnLimparPesquisa);
            this.tabPesquisa.Controls.Add(this.gpConfiguracoesPesquisa);
            this.tabPesquisa.Controls.Add(this.btnPesquisar);
            this.tabPesquisa.Controls.Add(this.edtUsuario);
            this.tabPesquisa.Controls.Add(this.cbTipoLancamento);
            this.tabPesquisa.Controls.Add(this.dtDataVencimento);
            this.tabPesquisa.Controls.Add(this.edtLancamento);
            this.tabPesquisa.Controls.Add(this.lblUsuario);
            this.tabPesquisa.Controls.Add(this.lblTipoLancamento);
            this.tabPesquisa.Controls.Add(this.lblDataVencimento);
            this.tabPesquisa.Controls.Add(this.lblLancamento);
            this.tabPesquisa.Font = new System.Drawing.Font("Hack", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tabPesquisa.Location = new System.Drawing.Point(4, 22);
            this.tabPesquisa.Name = "tabPesquisa";
            this.tabPesquisa.Padding = new System.Windows.Forms.Padding(3);
            this.tabPesquisa.Size = new System.Drawing.Size(452, 212);
            this.tabPesquisa.TabIndex = 0;
            this.tabPesquisa.Text = "Pesquisa";
            // 
            // gpConfiguracoesPesquisa
            // 
            this.gpConfiguracoesPesquisa.Controls.Add(this.chk100Compativel);
            this.gpConfiguracoesPesquisa.Controls.Add(this.chkConsiderarData);
            this.gpConfiguracoesPesquisa.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.gpConfiguracoesPesquisa.Font = new System.Drawing.Font("Hack", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.gpConfiguracoesPesquisa.Location = new System.Drawing.Point(304, 12);
            this.gpConfiguracoesPesquisa.Name = "gpConfiguracoesPesquisa";
            this.gpConfiguracoesPesquisa.Size = new System.Drawing.Size(145, 169);
            this.gpConfiguracoesPesquisa.TabIndex = 10;
            this.gpConfiguracoesPesquisa.TabStop = false;
            this.gpConfiguracoesPesquisa.Text = "Configurações da Pesquisa";
            // 
            // chk100Compativel
            // 
            this.chk100Compativel.AutoSize = true;
            this.chk100Compativel.Checked = true;
            this.chk100Compativel.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chk100Compativel.Location = new System.Drawing.Point(15, 43);
            this.chk100Compativel.Name = "chk100Compativel";
            this.chk100Compativel.Size = new System.Drawing.Size(99, 14);
            this.chk100Compativel.TabIndex = 2;
            this.chk100Compativel.Text = "100% Compatível";
            this.chk100Compativel.UseVisualStyleBackColor = true;
            // 
            // chkConsiderarData
            // 
            this.chkConsiderarData.AutoSize = true;
            this.chkConsiderarData.Checked = true;
            this.chkConsiderarData.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkConsiderarData.Location = new System.Drawing.Point(15, 19);
            this.chkConsiderarData.Name = "chkConsiderarData";
            this.chkConsiderarData.Size = new System.Drawing.Size(99, 14);
            this.chkConsiderarData.TabIndex = 0;
            this.chkConsiderarData.Text = "Considerar Data";
            this.chkConsiderarData.UseVisualStyleBackColor = true;
            // 
            // btnPesquisar
            // 
            this.btnPesquisar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnPesquisar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnPesquisar.Image = ((System.Drawing.Image)(resources.GetObject("btnPesquisar.Image")));
            this.btnPesquisar.Location = new System.Drawing.Point(189, 164);
            this.btnPesquisar.Name = "btnPesquisar";
            this.btnPesquisar.Size = new System.Drawing.Size(109, 26);
            this.btnPesquisar.TabIndex = 8;
            this.btnPesquisar.Text = "Pesquisar";
            this.btnPesquisar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnPesquisar.UseVisualStyleBackColor = false;
            this.btnPesquisar.Click += new System.EventHandler(this.btnPesquisar_Click);
            // 
            // edtUsuario
            // 
            this.edtUsuario.Location = new System.Drawing.Point(177, 125);
            this.edtUsuario.Name = "edtUsuario";
            this.edtUsuario.Size = new System.Drawing.Size(121, 20);
            this.edtUsuario.TabIndex = 7;
            // 
            // cbTipoLancamento
            // 
            this.cbTipoLancamento.FormattingEnabled = true;
            this.cbTipoLancamento.Items.AddRange(new object[] {
            "Crédito",
            "Débito"});
            this.cbTipoLancamento.Location = new System.Drawing.Point(177, 86);
            this.cbTipoLancamento.Name = "cbTipoLancamento";
            this.cbTipoLancamento.Size = new System.Drawing.Size(121, 21);
            this.cbTipoLancamento.TabIndex = 6;
            // 
            // dtDataVencimento
            // 
            this.dtDataVencimento.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDataVencimento.Location = new System.Drawing.Point(177, 47);
            this.dtDataVencimento.Name = "dtDataVencimento";
            this.dtDataVencimento.Size = new System.Drawing.Size(121, 20);
            this.dtDataVencimento.TabIndex = 5;
            // 
            // edtLancamento
            // 
            this.edtLancamento.Location = new System.Drawing.Point(105, 12);
            this.edtLancamento.Name = "edtLancamento";
            this.edtLancamento.Size = new System.Drawing.Size(193, 20);
            this.edtLancamento.TabIndex = 4;
            // 
            // lblUsuario
            // 
            this.lblUsuario.AutoSize = true;
            this.lblUsuario.Location = new System.Drawing.Point(15, 129);
            this.lblUsuario.Name = "lblUsuario";
            this.lblUsuario.Size = new System.Drawing.Size(56, 13);
            this.lblUsuario.TabIndex = 3;
            this.lblUsuario.Text = "Usuário";
            // 
            // lblTipoLancamento
            // 
            this.lblTipoLancamento.AutoSize = true;
            this.lblTipoLancamento.Location = new System.Drawing.Point(15, 90);
            this.lblTipoLancamento.Name = "lblTipoLancamento";
            this.lblTipoLancamento.Size = new System.Drawing.Size(140, 13);
            this.lblTipoLancamento.TabIndex = 2;
            this.lblTipoLancamento.Text = "Tipo do Lançamento:";
            // 
            // lblDataVencimento
            // 
            this.lblDataVencimento.AutoSize = true;
            this.lblDataVencimento.Location = new System.Drawing.Point(15, 51);
            this.lblDataVencimento.Name = "lblDataVencimento";
            this.lblDataVencimento.Size = new System.Drawing.Size(140, 13);
            this.lblDataVencimento.TabIndex = 1;
            this.lblDataVencimento.Text = "Data de Vencimento:";
            // 
            // lblLancamento
            // 
            this.lblLancamento.AutoSize = true;
            this.lblLancamento.Location = new System.Drawing.Point(15, 16);
            this.lblLancamento.Name = "lblLancamento";
            this.lblLancamento.Size = new System.Drawing.Size(84, 13);
            this.lblLancamento.TabIndex = 0;
            this.lblLancamento.Text = "Lançamento:";
            // 
            // tabResultados
            // 
            this.tabResultados.BackColor = System.Drawing.Color.PowderBlue;
            this.tabResultados.Controls.Add(this.btnVoltar);
            this.tabResultados.Controls.Add(this.edtTipoLancamentoResultado);
            this.tabResultados.Controls.Add(this.dtDataVencimentoResultado);
            this.tabResultados.Controls.Add(this.edtValor);
            this.tabResultados.Controls.Add(this.edtUsuarioResultado);
            this.tabResultados.Controls.Add(this.lblUsuarioResultado);
            this.tabResultados.Controls.Add(this.lblTipoLancamentoResultado);
            this.tabResultados.Controls.Add(this.lblDataVencimentoResultado);
            this.tabResultados.Controls.Add(this.lblValorResultado);
            this.tabResultados.Controls.Add(this.lblResultados);
            this.tabResultados.Controls.Add(this.cbResultados);
            this.tabResultados.Location = new System.Drawing.Point(4, 22);
            this.tabResultados.Name = "tabResultados";
            this.tabResultados.Padding = new System.Windows.Forms.Padding(3);
            this.tabResultados.Size = new System.Drawing.Size(452, 212);
            this.tabResultados.TabIndex = 1;
            this.tabResultados.Text = "Resultados";
            // 
            // dtDataVencimentoResultado
            // 
            this.dtDataVencimentoResultado.Enabled = false;
            this.dtDataVencimentoResultado.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.dtDataVencimentoResultado.Location = new System.Drawing.Point(308, 74);
            this.dtDataVencimentoResultado.Name = "dtDataVencimentoResultado";
            this.dtDataVencimentoResultado.Size = new System.Drawing.Size(121, 20);
            this.dtDataVencimentoResultado.TabIndex = 10;
            // 
            // edtValor
            // 
            this.edtValor.Location = new System.Drawing.Point(308, 39);
            this.edtValor.Name = "edtValor";
            this.edtValor.ReadOnly = true;
            this.edtValor.Size = new System.Drawing.Size(121, 20);
            this.edtValor.TabIndex = 8;
            // 
            // edtUsuarioResultado
            // 
            this.edtUsuarioResultado.Location = new System.Drawing.Point(308, 144);
            this.edtUsuarioResultado.Name = "edtUsuarioResultado";
            this.edtUsuarioResultado.ReadOnly = true;
            this.edtUsuarioResultado.Size = new System.Drawing.Size(121, 20);
            this.edtUsuarioResultado.TabIndex = 7;
            // 
            // lblUsuarioResultado
            // 
            this.lblUsuarioResultado.AutoSize = true;
            this.lblUsuarioResultado.Location = new System.Drawing.Point(16, 148);
            this.lblUsuarioResultado.Name = "lblUsuarioResultado";
            this.lblUsuarioResultado.Size = new System.Drawing.Size(63, 13);
            this.lblUsuarioResultado.TabIndex = 6;
            this.lblUsuarioResultado.Text = "Usuário:";
            // 
            // lblTipoLancamentoResultado
            // 
            this.lblTipoLancamentoResultado.AutoSize = true;
            this.lblTipoLancamentoResultado.Location = new System.Drawing.Point(16, 113);
            this.lblTipoLancamentoResultado.Name = "lblTipoLancamentoResultado";
            this.lblTipoLancamentoResultado.Size = new System.Drawing.Size(140, 13);
            this.lblTipoLancamentoResultado.TabIndex = 5;
            this.lblTipoLancamentoResultado.Text = "Tipo do Lançamento:";
            // 
            // lblDataVencimentoResultado
            // 
            this.lblDataVencimentoResultado.AutoSize = true;
            this.lblDataVencimentoResultado.Location = new System.Drawing.Point(16, 78);
            this.lblDataVencimentoResultado.Name = "lblDataVencimentoResultado";
            this.lblDataVencimentoResultado.Size = new System.Drawing.Size(140, 13);
            this.lblDataVencimentoResultado.TabIndex = 4;
            this.lblDataVencimentoResultado.Text = "Data de Vencimento:";
            // 
            // lblValorResultado
            // 
            this.lblValorResultado.AutoSize = true;
            this.lblValorResultado.Location = new System.Drawing.Point(16, 43);
            this.lblValorResultado.Name = "lblValorResultado";
            this.lblValorResultado.Size = new System.Drawing.Size(49, 13);
            this.lblValorResultado.TabIndex = 3;
            this.lblValorResultado.Text = "Valor:";
            // 
            // lblResultados
            // 
            this.lblResultados.AutoSize = true;
            this.lblResultados.Location = new System.Drawing.Point(11, 10);
            this.lblResultados.Name = "lblResultados";
            this.lblResultados.Size = new System.Drawing.Size(77, 13);
            this.lblResultados.TabIndex = 1;
            this.lblResultados.Text = "Resultado:";
            // 
            // cbResultados
            // 
            this.cbResultados.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cbResultados.FormattingEnabled = true;
            this.cbResultados.Location = new System.Drawing.Point(100, 6);
            this.cbResultados.Name = "cbResultados";
            this.cbResultados.Size = new System.Drawing.Size(344, 21);
            this.cbResultados.TabIndex = 0;
            this.cbResultados.TextChanged += new System.EventHandler(this.cbResultados_TextChanged);
            // 
            // edtTipoLancamentoResultado
            // 
            this.edtTipoLancamentoResultado.Location = new System.Drawing.Point(308, 110);
            this.edtTipoLancamentoResultado.Name = "edtTipoLancamentoResultado";
            this.edtTipoLancamentoResultado.ReadOnly = true;
            this.edtTipoLancamentoResultado.Size = new System.Drawing.Size(121, 20);
            this.edtTipoLancamentoResultado.TabIndex = 11;
            // 
            // btnLimparPesquisa
            // 
            this.btnLimparPesquisa.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnLimparPesquisa.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnLimparPesquisa.Image = ((System.Drawing.Image)(resources.GetObject("btnLimparPesquisa.Image")));
            this.btnLimparPesquisa.Location = new System.Drawing.Point(46, 164);
            this.btnLimparPesquisa.Name = "btnLimparPesquisa";
            this.btnLimparPesquisa.Size = new System.Drawing.Size(109, 26);
            this.btnLimparPesquisa.TabIndex = 11;
            this.btnLimparPesquisa.Text = "Limpar";
            this.btnLimparPesquisa.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnLimparPesquisa.UseVisualStyleBackColor = false;
            this.btnLimparPesquisa.Click += new System.EventHandler(this.btnLimparPesquisa_Click);
            // 
            // btnVoltar
            // 
            this.btnVoltar.BackColor = System.Drawing.Color.WhiteSmoke;
            this.btnVoltar.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnVoltar.Image = ((System.Drawing.Image)(resources.GetObject("btnVoltar.Image")));
            this.btnVoltar.Location = new System.Drawing.Point(320, 178);
            this.btnVoltar.Name = "btnVoltar";
            this.btnVoltar.Size = new System.Drawing.Size(109, 26);
            this.btnVoltar.TabIndex = 12;
            this.btnVoltar.Text = "Voltar";
            this.btnVoltar.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.btnVoltar.UseVisualStyleBackColor = false;
            this.btnVoltar.Click += new System.EventHandler(this.btnVoltar_Click);
            // 
            // Pesquisa
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(460, 238);
            this.Controls.Add(this.pgcFormPesquisa);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "Pesquisa";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Pesquisa";
            this.pgcFormPesquisa.ResumeLayout(false);
            this.tabPesquisa.ResumeLayout(false);
            this.tabPesquisa.PerformLayout();
            this.gpConfiguracoesPesquisa.ResumeLayout(false);
            this.gpConfiguracoesPesquisa.PerformLayout();
            this.tabResultados.ResumeLayout(false);
            this.tabResultados.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl pgcFormPesquisa;
        private System.Windows.Forms.TabPage tabPesquisa;
        private System.Windows.Forms.TabPage tabResultados;
        private System.Windows.Forms.Button btnPesquisar;
        private System.Windows.Forms.TextBox edtUsuario;
        private System.Windows.Forms.ComboBox cbTipoLancamento;
        private System.Windows.Forms.DateTimePicker dtDataVencimento;
        private System.Windows.Forms.TextBox edtLancamento;
        private System.Windows.Forms.Label lblUsuario;
        private System.Windows.Forms.Label lblTipoLancamento;
        private System.Windows.Forms.Label lblDataVencimento;
        private System.Windows.Forms.Label lblLancamento;
        private System.Windows.Forms.Label lblTipoLancamentoResultado;
        private System.Windows.Forms.Label lblDataVencimentoResultado;
        private System.Windows.Forms.Label lblValorResultado;
        private System.Windows.Forms.Label lblResultados;
        private System.Windows.Forms.ComboBox cbResultados;
        private System.Windows.Forms.Label lblUsuarioResultado;
        private System.Windows.Forms.DateTimePicker dtDataVencimentoResultado;
        private System.Windows.Forms.TextBox edtValor;
        private System.Windows.Forms.TextBox edtUsuarioResultado;
        private System.Windows.Forms.GroupBox gpConfiguracoesPesquisa;
        private System.Windows.Forms.CheckBox chk100Compativel;
        private System.Windows.Forms.CheckBox chkConsiderarData;
        private System.Windows.Forms.TextBox edtTipoLancamentoResultado;
        private System.Windows.Forms.Button btnLimparPesquisa;
        private System.Windows.Forms.Button btnVoltar;
    }
}