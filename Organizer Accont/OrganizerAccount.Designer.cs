﻿namespace Organizer_Account
{
    partial class OrganizerAccount
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(OrganizerAccount));
            this.menuPrincipal = new System.Windows.Forms.MenuStrip();
            this.btnArquivo = new System.Windows.Forms.ToolStripMenuItem();
            this.btnNovo = new System.Windows.Forms.ToolStripMenuItem();
            this.btnAbrir = new System.Windows.Forms.ToolStripMenuItem();
            this.btnSalvar = new System.Windows.Forms.ToolStripMenuItem();
            this.btnFechar = new System.Windows.Forms.ToolStripMenuItem();
            this.btnConfiguracoes = new System.Windows.Forms.ToolStripMenuItem();
            this.pnlGridLancamentos = new System.Windows.Forms.Panel();
            this.gridLancamentos = new System.Windows.Forms.DataGridView();
            this.lblTotalizadores = new System.Windows.Forms.Label();
            this.lblTotalCredito = new System.Windows.Forms.Label();
            this.lblTotalDebito = new System.Windows.Forms.Label();
            this.lblTotalGeral = new System.Windows.Forms.Label();
            this.lblContadorCredito = new System.Windows.Forms.Label();
            this.lblContadorDebito = new System.Windows.Forms.Label();
            this.lblContadorTotalGeral = new System.Windows.Forms.Label();
            this.pnlTotalizadores = new System.Windows.Forms.Panel();
            this.btnTotalizar = new System.Windows.Forms.Button();
            this.pnlDescricaoLancamento = new System.Windows.Forms.Panel();
            this.edtData = new System.Windows.Forms.DateTimePicker();
            this.lblDataLancamento = new System.Windows.Forms.Label();
            this.btnDeletar = new System.Windows.Forms.Button();
            this.btnAlterar = new System.Windows.Forms.Button();
            this.btnAdicionar = new System.Windows.Forms.Button();
            this.cbTipoCredito = new System.Windows.Forms.ComboBox();
            this.lblTipoLancamento = new System.Windows.Forms.Label();
            this.edtValorLancamento = new System.Windows.Forms.TextBox();
            this.lblValor = new System.Windows.Forms.Label();
            this.edtLancamento = new System.Windows.Forms.TextBox();
            this.lblLancamento = new System.Windows.Forms.Label();
            this.saveFileOrganizerAccount = new System.Windows.Forms.SaveFileDialog();
            this.openFileOrganizerAccount = new System.Windows.Forms.OpenFileDialog();
            this.menuPrincipal.SuspendLayout();
            this.pnlGridLancamentos.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.gridLancamentos)).BeginInit();
            this.pnlTotalizadores.SuspendLayout();
            this.pnlDescricaoLancamento.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuPrincipal
            // 
            this.menuPrincipal.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnArquivo,
            this.btnConfiguracoes});
            this.menuPrincipal.Location = new System.Drawing.Point(0, 0);
            this.menuPrincipal.Name = "menuPrincipal";
            this.menuPrincipal.Size = new System.Drawing.Size(800, 24);
            this.menuPrincipal.TabIndex = 0;
            this.menuPrincipal.Text = "menuPrincipal";
            // 
            // btnArquivo
            // 
            this.btnArquivo.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.btnNovo,
            this.btnAbrir,
            this.btnSalvar,
            this.btnFechar});
            this.btnArquivo.Name = "btnArquivo";
            this.btnArquivo.Size = new System.Drawing.Size(61, 20);
            this.btnArquivo.Text = "Arquivo";
            // 
            // btnNovo
            // 
            this.btnNovo.Name = "btnNovo";
            this.btnNovo.Size = new System.Drawing.Size(109, 22);
            this.btnNovo.Text = "Novo";
            this.btnNovo.Click += new System.EventHandler(this.btnNovo_Click);
            // 
            // btnAbrir
            // 
            this.btnAbrir.Name = "btnAbrir";
            this.btnAbrir.Size = new System.Drawing.Size(109, 22);
            this.btnAbrir.Text = "Abrir";
            this.btnAbrir.Click += new System.EventHandler(this.btnAbrir_Click);
            // 
            // btnSalvar
            // 
            this.btnSalvar.Name = "btnSalvar";
            this.btnSalvar.Size = new System.Drawing.Size(109, 22);
            this.btnSalvar.Text = "Salvar";
            this.btnSalvar.Click += new System.EventHandler(this.btnSalvar_Click);
            // 
            // btnFechar
            // 
            this.btnFechar.Name = "btnFechar";
            this.btnFechar.Size = new System.Drawing.Size(109, 22);
            this.btnFechar.Text = "Fechar";
            this.btnFechar.Click += new System.EventHandler(this.btnFechar_Click);
            // 
            // btnConfiguracoes
            // 
            this.btnConfiguracoes.Name = "btnConfiguracoes";
            this.btnConfiguracoes.Size = new System.Drawing.Size(96, 20);
            this.btnConfiguracoes.Text = "Configurações";
            this.btnConfiguracoes.Click += new System.EventHandler(this.btnConfiguracoes_Click);
            // 
            // pnlGridLancamentos
            // 
            this.pnlGridLancamentos.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlGridLancamentos.Controls.Add(this.gridLancamentos);
            this.pnlGridLancamentos.Location = new System.Drawing.Point(0, 95);
            this.pnlGridLancamentos.Name = "pnlGridLancamentos";
            this.pnlGridLancamentos.Size = new System.Drawing.Size(565, 285);
            this.pnlGridLancamentos.TabIndex = 2;
            // 
            // gridLancamentos
            // 
            this.gridLancamentos.BackgroundColor = System.Drawing.SystemColors.ControlLightLight;
            this.gridLancamentos.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.gridLancamentos.Location = new System.Drawing.Point(3, 3);
            this.gridLancamentos.Name = "gridLancamentos";
            this.gridLancamentos.ReadOnly = true;
            this.gridLancamentos.Size = new System.Drawing.Size(557, 277);
            this.gridLancamentos.TabIndex = 0;
            // 
            // lblTotalizadores
            // 
            this.lblTotalizadores.AutoSize = true;
            this.lblTotalizadores.Font = new System.Drawing.Font("F25 Bank Printer", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalizadores.Location = new System.Drawing.Point(29, 3);
            this.lblTotalizadores.Name = "lblTotalizadores";
            this.lblTotalizadores.Size = new System.Drawing.Size(149, 20);
            this.lblTotalizadores.TabIndex = 0;
            this.lblTotalizadores.Text = "Totalizadores";
            // 
            // lblTotalCredito
            // 
            this.lblTotalCredito.AutoSize = true;
            this.lblTotalCredito.Font = new System.Drawing.Font("F25 Bank Printer", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalCredito.Location = new System.Drawing.Point(3, 32);
            this.lblTotalCredito.Name = "lblTotalCredito";
            this.lblTotalCredito.Size = new System.Drawing.Size(107, 12);
            this.lblTotalCredito.TabIndex = 1;
            this.lblTotalCredito.Text = "Total de Crédito:";
            // 
            // lblTotalDebito
            // 
            this.lblTotalDebito.AutoSize = true;
            this.lblTotalDebito.Font = new System.Drawing.Font("F25 Bank Printer", 6.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalDebito.Location = new System.Drawing.Point(3, 64);
            this.lblTotalDebito.Name = "lblTotalDebito";
            this.lblTotalDebito.Size = new System.Drawing.Size(101, 12);
            this.lblTotalDebito.TabIndex = 2;
            this.lblTotalDebito.Text = "Total de Débito:";
            // 
            // lblTotalGeral
            // 
            this.lblTotalGeral.AutoSize = true;
            this.lblTotalGeral.Font = new System.Drawing.Font("F25 Bank Printer", 6.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotalGeral.Location = new System.Drawing.Point(3, 96);
            this.lblTotalGeral.Name = "lblTotalGeral";
            this.lblTotalGeral.Size = new System.Drawing.Size(77, 12);
            this.lblTotalGeral.TabIndex = 3;
            this.lblTotalGeral.Text = "Total Geral:";
            // 
            // lblContadorCredito
            // 
            this.lblContadorCredito.AutoSize = true;
            this.lblContadorCredito.Font = new System.Drawing.Font("F25 Bank Printer", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContadorCredito.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.lblContadorCredito.Location = new System.Drawing.Point(104, 31);
            this.lblContadorCredito.Name = "lblContadorCredito";
            this.lblContadorCredito.Size = new System.Drawing.Size(22, 14);
            this.lblContadorCredito.TabIndex = 4;
            this.lblContadorCredito.Text = "R$";
            // 
            // lblContadorDebito
            // 
            this.lblContadorDebito.AutoSize = true;
            this.lblContadorDebito.Font = new System.Drawing.Font("F25 Bank Printer", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContadorDebito.ForeColor = System.Drawing.Color.Maroon;
            this.lblContadorDebito.Location = new System.Drawing.Point(104, 63);
            this.lblContadorDebito.Name = "lblContadorDebito";
            this.lblContadorDebito.Size = new System.Drawing.Size(22, 14);
            this.lblContadorDebito.TabIndex = 5;
            this.lblContadorDebito.Text = "R$";
            // 
            // lblContadorTotalGeral
            // 
            this.lblContadorTotalGeral.AutoSize = true;
            this.lblContadorTotalGeral.Font = new System.Drawing.Font("F25 Bank Printer", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContadorTotalGeral.ForeColor = System.Drawing.Color.Black;
            this.lblContadorTotalGeral.Location = new System.Drawing.Point(104, 95);
            this.lblContadorTotalGeral.Name = "lblContadorTotalGeral";
            this.lblContadorTotalGeral.Size = new System.Drawing.Size(22, 14);
            this.lblContadorTotalGeral.TabIndex = 6;
            this.lblContadorTotalGeral.Text = "R$";
            // 
            // pnlTotalizadores
            // 
            this.pnlTotalizadores.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlTotalizadores.Controls.Add(this.btnTotalizar);
            this.pnlTotalizadores.Controls.Add(this.lblContadorTotalGeral);
            this.pnlTotalizadores.Controls.Add(this.lblContadorDebito);
            this.pnlTotalizadores.Controls.Add(this.lblContadorCredito);
            this.pnlTotalizadores.Controls.Add(this.lblTotalGeral);
            this.pnlTotalizadores.Controls.Add(this.lblTotalDebito);
            this.pnlTotalizadores.Controls.Add(this.lblTotalCredito);
            this.pnlTotalizadores.Controls.Add(this.lblTotalizadores);
            this.pnlTotalizadores.Location = new System.Drawing.Point(567, 95);
            this.pnlTotalizadores.Name = "pnlTotalizadores";
            this.pnlTotalizadores.Size = new System.Drawing.Size(232, 285);
            this.pnlTotalizadores.TabIndex = 3;
            // 
            // btnTotalizar
            // 
            this.btnTotalizar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnTotalizar.Location = new System.Drawing.Point(77, 115);
            this.btnTotalizar.Name = "btnTotalizar";
            this.btnTotalizar.Size = new System.Drawing.Size(70, 23);
            this.btnTotalizar.TabIndex = 7;
            this.btnTotalizar.Text = "Totalizar";
            this.btnTotalizar.UseVisualStyleBackColor = false;
            this.btnTotalizar.Click += new System.EventHandler(this.btnTotalizar_Click);
            // 
            // pnlDescricaoLancamento
            // 
            this.pnlDescricaoLancamento.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.pnlDescricaoLancamento.Controls.Add(this.edtData);
            this.pnlDescricaoLancamento.Controls.Add(this.lblDataLancamento);
            this.pnlDescricaoLancamento.Controls.Add(this.btnDeletar);
            this.pnlDescricaoLancamento.Controls.Add(this.btnAlterar);
            this.pnlDescricaoLancamento.Controls.Add(this.btnAdicionar);
            this.pnlDescricaoLancamento.Controls.Add(this.cbTipoCredito);
            this.pnlDescricaoLancamento.Controls.Add(this.lblTipoLancamento);
            this.pnlDescricaoLancamento.Controls.Add(this.edtValorLancamento);
            this.pnlDescricaoLancamento.Controls.Add(this.lblValor);
            this.pnlDescricaoLancamento.Controls.Add(this.edtLancamento);
            this.pnlDescricaoLancamento.Controls.Add(this.lblLancamento);
            this.pnlDescricaoLancamento.Location = new System.Drawing.Point(1, 27);
            this.pnlDescricaoLancamento.Name = "pnlDescricaoLancamento";
            this.pnlDescricaoLancamento.Size = new System.Drawing.Size(798, 66);
            this.pnlDescricaoLancamento.TabIndex = 4;
            // 
            // edtData
            // 
            this.edtData.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.edtData.Location = new System.Drawing.Point(710, 6);
            this.edtData.Name = "edtData";
            this.edtData.Size = new System.Drawing.Size(81, 20);
            this.edtData.TabIndex = 2;
            // 
            // lblDataLancamento
            // 
            this.lblDataLancamento.AutoSize = true;
            this.lblDataLancamento.Font = new System.Drawing.Font("F25 Bank Printer", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDataLancamento.Location = new System.Drawing.Point(565, 10);
            this.lblDataLancamento.Name = "lblDataLancamento";
            this.lblDataLancamento.Size = new System.Drawing.Size(148, 14);
            this.lblDataLancamento.TabIndex = 10;
            this.lblDataLancamento.Text = "Data de Vencimento:";
            // 
            // btnDeletar
            // 
            this.btnDeletar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDeletar.Location = new System.Drawing.Point(643, 32);
            this.btnDeletar.Name = "btnDeletar";
            this.btnDeletar.Size = new System.Drawing.Size(70, 23);
            this.btnDeletar.TabIndex = 7;
            this.btnDeletar.Text = "Deletar";
            this.btnDeletar.UseVisualStyleBackColor = false;
            this.btnDeletar.Click += new System.EventHandler(this.btnDeletar_Click);
            // 
            // btnAlterar
            // 
            this.btnAlterar.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnAlterar.Location = new System.Drawing.Point(722, 32);
            this.btnAlterar.Name = "btnAlterar";
            this.btnAlterar.Size = new System.Drawing.Size(70, 23);
            this.btnAlterar.TabIndex = 8;
            this.btnAlterar.Text = "Alterar";
            this.btnAlterar.UseVisualStyleBackColor = false;
            this.btnAlterar.Click += new System.EventHandler(this.btnAlterar_Click);
            // 
            // btnAdicionar
            // 
            this.btnAdicionar.BackColor = System.Drawing.SystemColors.ButtonHighlight;
            this.btnAdicionar.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnAdicionar.Location = new System.Drawing.Point(564, 32);
            this.btnAdicionar.Name = "btnAdicionar";
            this.btnAdicionar.Size = new System.Drawing.Size(70, 23);
            this.btnAdicionar.TabIndex = 5;
            this.btnAdicionar.Text = "Adicionar";
            this.btnAdicionar.UseVisualStyleBackColor = false;
            this.btnAdicionar.Click += new System.EventHandler(this.btnAdicionar_Click);
            // 
            // cbTipoCredito
            // 
            this.cbTipoCredito.FormattingEnabled = true;
            this.cbTipoCredito.Items.AddRange(new object[] {
            "Crédito",
            "Débito"});
            this.cbTipoCredito.Location = new System.Drawing.Point(438, 33);
            this.cbTipoCredito.Name = "cbTipoCredito";
            this.cbTipoCredito.Size = new System.Drawing.Size(121, 21);
            this.cbTipoCredito.TabIndex = 4;
            // 
            // lblTipoLancamento
            // 
            this.lblTipoLancamento.AutoSize = true;
            this.lblTipoLancamento.Font = new System.Drawing.Font("F25 Bank Printer", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTipoLancamento.Location = new System.Drawing.Point(282, 36);
            this.lblTipoLancamento.Name = "lblTipoLancamento";
            this.lblTipoLancamento.Size = new System.Drawing.Size(150, 14);
            this.lblTipoLancamento.TabIndex = 4;
            this.lblTipoLancamento.Text = "Tipo do Lançamento:";
            // 
            // edtValorLancamento
            // 
            this.edtValorLancamento.Location = new System.Drawing.Point(99, 33);
            this.edtValorLancamento.Name = "edtValorLancamento";
            this.edtValorLancamento.Size = new System.Drawing.Size(130, 20);
            this.edtValorLancamento.TabIndex = 3;
            this.edtValorLancamento.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.edtValorLancamento_KeyPress);
            // 
            // lblValor
            // 
            this.lblValor.AutoSize = true;
            this.lblValor.Font = new System.Drawing.Font("F25 Bank Printer", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblValor.Location = new System.Drawing.Point(10, 36);
            this.lblValor.Name = "lblValor";
            this.lblValor.Size = new System.Drawing.Size(88, 14);
            this.lblValor.TabIndex = 2;
            this.lblValor.Text = "Valor (R$):";
            // 
            // edtLancamento
            // 
            this.edtLancamento.Location = new System.Drawing.Point(99, 6);
            this.edtLancamento.Name = "edtLancamento";
            this.edtLancamento.Size = new System.Drawing.Size(460, 20);
            this.edtLancamento.TabIndex = 1;
            // 
            // lblLancamento
            // 
            this.lblLancamento.AutoSize = true;
            this.lblLancamento.Font = new System.Drawing.Font("F25 Bank Printer", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLancamento.Location = new System.Drawing.Point(10, 9);
            this.lblLancamento.Name = "lblLancamento";
            this.lblLancamento.Size = new System.Drawing.Size(90, 14);
            this.lblLancamento.TabIndex = 0;
            this.lblLancamento.Text = "Lançamento:";
            // 
            // OrganizerAccount
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.ClientSize = new System.Drawing.Size(800, 383);
            this.Controls.Add(this.pnlDescricaoLancamento);
            this.Controls.Add(this.pnlTotalizadores);
            this.Controls.Add(this.pnlGridLancamentos);
            this.Controls.Add(this.menuPrincipal);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuPrincipal;
            this.Name = "OrganizerAccount";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Organizer Account v0.01";
            this.menuPrincipal.ResumeLayout(false);
            this.menuPrincipal.PerformLayout();
            this.pnlGridLancamentos.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.gridLancamentos)).EndInit();
            this.pnlTotalizadores.ResumeLayout(false);
            this.pnlTotalizadores.PerformLayout();
            this.pnlDescricaoLancamento.ResumeLayout(false);
            this.pnlDescricaoLancamento.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuPrincipal;
        private System.Windows.Forms.ToolStripMenuItem btnConfiguracoes;
        private System.Windows.Forms.ToolStripMenuItem btnArquivo;
        private System.Windows.Forms.ToolStripMenuItem btnNovo;
        private System.Windows.Forms.ToolStripMenuItem btnAbrir;
        private System.Windows.Forms.ToolStripMenuItem btnFechar;
        private System.Windows.Forms.Panel pnlGridLancamentos;
        private System.Windows.Forms.Label lblTotalizadores;
        private System.Windows.Forms.Label lblTotalCredito;
        private System.Windows.Forms.Label lblTotalDebito;
        private System.Windows.Forms.Label lblTotalGeral;
        private System.Windows.Forms.Label lblContadorCredito;
        private System.Windows.Forms.Label lblContadorDebito;
        private System.Windows.Forms.Label lblContadorTotalGeral;
        private System.Windows.Forms.Panel pnlTotalizadores;
        private System.Windows.Forms.Panel pnlDescricaoLancamento;
        private System.Windows.Forms.Label lblValor;
        private System.Windows.Forms.TextBox edtLancamento;
        private System.Windows.Forms.Label lblLancamento;
        private System.Windows.Forms.TextBox edtValorLancamento;
        private System.Windows.Forms.Label lblTipoLancamento;
        private System.Windows.Forms.ComboBox cbTipoCredito;
        private System.Windows.Forms.Button btnDeletar;
        private System.Windows.Forms.Button btnAlterar;
        private System.Windows.Forms.Button btnAdicionar;
        private System.Windows.Forms.ToolStripMenuItem btnSalvar;
        private System.Windows.Forms.Label lblDataLancamento;
        private System.Windows.Forms.SaveFileDialog saveFileOrganizerAccount;
        private System.Windows.Forms.DataGridView gridLancamentos;
        private System.Windows.Forms.OpenFileDialog openFileOrganizerAccount;
        private System.Windows.Forms.DateTimePicker edtData;
        private System.Windows.Forms.Button btnTotalizar;
    }
}