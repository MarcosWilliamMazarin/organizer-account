﻿namespace Organizer_Account
{
    partial class FormConfiguracoes
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(FormConfiguracoes));
            this.btnDiretorioPadrao = new System.Windows.Forms.Button();
            this.fdlDiretorioPadrao = new System.Windows.Forms.FolderBrowserDialog();
            this.edtUsuarioPadrao = new System.Windows.Forms.TextBox();
            this.lblUsuarioPadrao = new System.Windows.Forms.Label();
            this.edtDiasAlerta = new System.Windows.Forms.NumericUpDown();
            this.lblDiasAlerta = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.pnlConfiguracoesGerais = new System.Windows.Forms.Panel();
            this.pnlAvisos = new System.Windows.Forms.Panel();
            this.pgcConfiguracoes = new System.Windows.Forms.TabControl();
            this.tabConfiguracoesGerais = new System.Windows.Forms.TabPage();
            this.tabAvisos = new System.Windows.Forms.TabPage();
            ((System.ComponentModel.ISupportInitialize)(this.edtDiasAlerta)).BeginInit();
            this.pnlConfiguracoesGerais.SuspendLayout();
            this.pnlAvisos.SuspendLayout();
            this.pgcConfiguracoes.SuspendLayout();
            this.tabConfiguracoesGerais.SuspendLayout();
            this.tabAvisos.SuspendLayout();
            this.SuspendLayout();
            // 
            // btnDiretorioPadrao
            // 
            this.btnDiretorioPadrao.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.btnDiretorioPadrao.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.btnDiretorioPadrao.Location = new System.Drawing.Point(84, 17);
            this.btnDiretorioPadrao.Name = "btnDiretorioPadrao";
            this.btnDiretorioPadrao.Size = new System.Drawing.Size(191, 26);
            this.btnDiretorioPadrao.TabIndex = 0;
            this.btnDiretorioPadrao.Text = "Definir Diretório Padrão";
            this.btnDiretorioPadrao.UseVisualStyleBackColor = false;
            this.btnDiretorioPadrao.Click += new System.EventHandler(this.btnDiretorioPadrao_Click);
            // 
            // edtUsuarioPadrao
            // 
            this.edtUsuarioPadrao.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.edtUsuarioPadrao.Location = new System.Drawing.Point(173, 50);
            this.edtUsuarioPadrao.Name = "edtUsuarioPadrao";
            this.edtUsuarioPadrao.Size = new System.Drawing.Size(102, 20);
            this.edtUsuarioPadrao.TabIndex = 2;
            // 
            // lblUsuarioPadrao
            // 
            this.lblUsuarioPadrao.AutoSize = true;
            this.lblUsuarioPadrao.Location = new System.Drawing.Point(81, 53);
            this.lblUsuarioPadrao.Name = "lblUsuarioPadrao";
            this.lblUsuarioPadrao.Size = new System.Drawing.Size(86, 13);
            this.lblUsuarioPadrao.TabIndex = 3;
            this.lblUsuarioPadrao.Text = "Usuário Padrão :";
            this.lblUsuarioPadrao.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // edtDiasAlerta
            // 
            this.edtDiasAlerta.Location = new System.Drawing.Point(156, 10);
            this.edtDiasAlerta.Name = "edtDiasAlerta";
            this.edtDiasAlerta.Size = new System.Drawing.Size(32, 20);
            this.edtDiasAlerta.TabIndex = 4;
            // 
            // lblDiasAlerta
            // 
            this.lblDiasAlerta.AutoSize = true;
            this.lblDiasAlerta.Location = new System.Drawing.Point(27, 12);
            this.lblDiasAlerta.Name = "lblDiasAlerta";
            this.lblDiasAlerta.Size = new System.Drawing.Size(129, 13);
            this.lblDiasAlerta.TabIndex = 5;
            this.lblDiasAlerta.Text = "Alertar vencimentos  com:";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(188, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(109, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "dias de antecedência";
            // 
            // pnlConfiguracoesGerais
            // 
            this.pnlConfiguracoesGerais.BackColor = System.Drawing.Color.PowderBlue;
            this.pnlConfiguracoesGerais.Controls.Add(this.btnDiretorioPadrao);
            this.pnlConfiguracoesGerais.Controls.Add(this.edtUsuarioPadrao);
            this.pnlConfiguracoesGerais.Controls.Add(this.lblUsuarioPadrao);
            this.pnlConfiguracoesGerais.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlConfiguracoesGerais.Location = new System.Drawing.Point(3, 3);
            this.pnlConfiguracoesGerais.Name = "pnlConfiguracoesGerais";
            this.pnlConfiguracoesGerais.Size = new System.Drawing.Size(357, 92);
            this.pnlConfiguracoesGerais.TabIndex = 7;
            // 
            // pnlAvisos
            // 
            this.pnlAvisos.BackColor = System.Drawing.Color.PowderBlue;
            this.pnlAvisos.Controls.Add(this.lblDiasAlerta);
            this.pnlAvisos.Controls.Add(this.edtDiasAlerta);
            this.pnlAvisos.Controls.Add(this.label1);
            this.pnlAvisos.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pnlAvisos.Location = new System.Drawing.Point(3, 3);
            this.pnlAvisos.Name = "pnlAvisos";
            this.pnlAvisos.Size = new System.Drawing.Size(357, 92);
            this.pnlAvisos.TabIndex = 8;
            // 
            // pgcConfiguracoes
            // 
            this.pgcConfiguracoes.Controls.Add(this.tabConfiguracoesGerais);
            this.pgcConfiguracoes.Controls.Add(this.tabAvisos);
            this.pgcConfiguracoes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pgcConfiguracoes.Location = new System.Drawing.Point(0, 0);
            this.pgcConfiguracoes.Name = "pgcConfiguracoes";
            this.pgcConfiguracoes.SelectedIndex = 0;
            this.pgcConfiguracoes.Size = new System.Drawing.Size(371, 124);
            this.pgcConfiguracoes.TabIndex = 9;
            // 
            // tabConfiguracoesGerais
            // 
            this.tabConfiguracoesGerais.Controls.Add(this.pnlConfiguracoesGerais);
            this.tabConfiguracoesGerais.Location = new System.Drawing.Point(4, 22);
            this.tabConfiguracoesGerais.Name = "tabConfiguracoesGerais";
            this.tabConfiguracoesGerais.Padding = new System.Windows.Forms.Padding(3);
            this.tabConfiguracoesGerais.Size = new System.Drawing.Size(363, 98);
            this.tabConfiguracoesGerais.TabIndex = 0;
            this.tabConfiguracoesGerais.Text = "Configurações Gerais";
            this.tabConfiguracoesGerais.UseVisualStyleBackColor = true;
            // 
            // tabAvisos
            // 
            this.tabAvisos.Controls.Add(this.pnlAvisos);
            this.tabAvisos.Location = new System.Drawing.Point(4, 22);
            this.tabAvisos.Name = "tabAvisos";
            this.tabAvisos.Padding = new System.Windows.Forms.Padding(3);
            this.tabAvisos.Size = new System.Drawing.Size(363, 98);
            this.tabAvisos.TabIndex = 1;
            this.tabAvisos.Text = "Avisos";
            this.tabAvisos.UseVisualStyleBackColor = true;
            // 
            // FormAuxiliar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.PowderBlue;
            this.ClientSize = new System.Drawing.Size(371, 124);
            this.Controls.Add(this.pgcConfiguracoes);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "FormAuxiliar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Configurações";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.FormAuxiliar_FormClosed);
            ((System.ComponentModel.ISupportInitialize)(this.edtDiasAlerta)).EndInit();
            this.pnlConfiguracoesGerais.ResumeLayout(false);
            this.pnlConfiguracoesGerais.PerformLayout();
            this.pnlAvisos.ResumeLayout(false);
            this.pnlAvisos.PerformLayout();
            this.pgcConfiguracoes.ResumeLayout(false);
            this.tabConfiguracoesGerais.ResumeLayout(false);
            this.tabAvisos.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnDiretorioPadrao;
        private System.Windows.Forms.FolderBrowserDialog fdlDiretorioPadrao;
        private System.Windows.Forms.TextBox edtUsuarioPadrao;
        private System.Windows.Forms.Label lblUsuarioPadrao;
        private System.Windows.Forms.NumericUpDown edtDiasAlerta;
        private System.Windows.Forms.Label lblDiasAlerta;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel pnlConfiguracoesGerais;
        private System.Windows.Forms.Panel pnlAvisos;
        private System.Windows.Forms.TabControl pgcConfiguracoes;
        private System.Windows.Forms.TabPage tabConfiguracoesGerais;
        private System.Windows.Forms.TabPage tabAvisos;
    }
}